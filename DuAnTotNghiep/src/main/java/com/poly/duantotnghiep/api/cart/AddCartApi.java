package com.poly.duantotnghiep.api.cart;


import com.poly.duantotnghiep.api.baseApi.IApi;
import com.poly.duantotnghiep.constant.ApplicationCode;
import com.poly.duantotnghiep.dto.request.BaseRequestData;
import com.poly.duantotnghiep.dto.response.baseReponse.BaseResponseData;
import com.poly.duantotnghiep.dto.response.baseReponse.IResponseData;
import com.poly.duantotnghiep.dto.response.cart.CartRespone;
import com.poly.duantotnghiep.service.CartService;
import com.poly.duantotnghiep.utils.exception.ApplicationException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("addCart")
@Log4j2
public class AddCartApi implements IApi {
    @Autowired
    CartService cartService;
    @Override
    public BaseResponseData excute(BaseRequestData request) throws ApplicationException {
        BaseResponseData<IResponseData> baseResponseData = new BaseResponseData<>();

        try {
            CartRespone cartRespone = cartService.addCart(request);
            if (cartRespone.getCartId()==0){
                baseResponseData.setErrorCode(ApplicationCode.ERROR);
                baseResponseData.setMessage(ApplicationCode.getMessage(ApplicationCode.ERROR));
                return baseResponseData;
            }
            baseResponseData.setErrorCode(ApplicationCode.SUCCESS);
            baseResponseData.setMessage(ApplicationCode.getMessage(ApplicationCode.SUCCESS));
            baseResponseData.setWsResponse(cartRespone);
            return baseResponseData;
        } catch (Exception e){
            baseResponseData.setErrorCode(ApplicationCode.ERROR);
            baseResponseData.setMessage(e.getMessage());
            return baseResponseData;
        }
    }
}
