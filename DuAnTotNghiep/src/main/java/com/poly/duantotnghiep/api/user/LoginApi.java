package com.poly.duantotnghiep.api.user;

import com.poly.duantotnghiep.api.baseApi.IApi;
import com.poly.duantotnghiep.constant.ApplicationCode;
import com.poly.duantotnghiep.dto.request.BaseRequestData;
import com.poly.duantotnghiep.dto.request.user.UserRequest;
import com.poly.duantotnghiep.dto.response.baseReponse.BaseResponseData;
import com.poly.duantotnghiep.dto.response.user.UserReponse;
import com.poly.duantotnghiep.repositoty.UserRepository;
import com.poly.duantotnghiep.service.UserService;
import com.poly.duantotnghiep.utils.exception.ApplicationException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Component(value = "loginCMS")
@Log4j2
public class LoginApi implements IApi {
    @Autowired
    UserService userService;
    @Autowired
    private HttpServletRequest requests;
    @Autowired
    UserRepository userRepository;
    @Override
    public BaseResponseData excute(BaseRequestData request) throws ApplicationException {
        UserRequest userRequest = (UserRequest) request.getWsRequest();
        BaseResponseData response = new BaseResponseData();
        try {
            UserReponse userRespone = userService.login(userRequest);
            if(ObjectUtils.isEmpty(userRespone)){
                response.setErrorCode(ApplicationCode.USER_EMPTY);
                response.setMessage(ApplicationCode.getMessage(ApplicationCode.USER_EMPTY));
                return response;
            }
            HttpSession session = requests.getSession();
            session.setMaxInactiveInterval(60*60*24);
            userRespone.setSession(session.getId());
            response.setErrorCode(ApplicationCode.SUCCESS);
            response.setMessage(ApplicationCode.getMessage(ApplicationCode.SUCCESS));
            response.setWsResponse(userRespone);
            userRepository.updateSession(session.getId(), userRequest.getUsername());
        } catch (ApplicationException e) {
            response.setErrorCode(e.getCode());
            response.setMessage(e.getMessage());
        } finally {
            log.info("\n Request :" + request);
            log.info("\n Response :" + response);
        }
        return response;
    }
}
