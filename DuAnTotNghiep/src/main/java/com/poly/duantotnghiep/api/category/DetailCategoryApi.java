package com.poly.duantotnghiep.api.category;

import com.poly.duantotnghiep.api.baseApi.IApi;
import com.poly.duantotnghiep.constant.ApplicationCode;
import com.poly.duantotnghiep.dto.request.BaseRequestData;
import com.poly.duantotnghiep.dto.response.baseReponse.BaseResponseData;
import com.poly.duantotnghiep.dto.response.baseReponse.IResponseData;
import com.poly.duantotnghiep.dto.response.brand.BrandResponse;
import com.poly.duantotnghiep.dto.response.category.CategoryResponse;
import com.poly.duantotnghiep.service.BrandService;
import com.poly.duantotnghiep.service.CategoryService;
import com.poly.duantotnghiep.utils.exception.ApplicationException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component(value = "detailCategory")
@Log4j2
public class DetailCategoryApi implements IApi {

    @Autowired

    CategoryService categoryService;
    @Override
    public BaseResponseData excute(BaseRequestData request) throws ApplicationException {
        BaseResponseData<IResponseData> baseResponseData = new BaseResponseData<>();
        try {
            CategoryResponse categoryResponse = categoryService.detailCategory(request);
            baseResponseData.setWsResponse(categoryResponse);
            baseResponseData.setErrorCode(ApplicationCode.SUCCESS);
            baseResponseData.setMessage(ApplicationCode.getMessage(ApplicationCode.SUCCESS));
        } catch (ApplicationException e) {
            baseResponseData.setErrorCode(e.getCode());
            baseResponseData.setMessage(ApplicationCode.getMessage(e.getCode()));
        } finally {
            log.info("\n Request :" + request);
            log.info("\n Response :" + baseResponseData);
        }
        return baseResponseData;
    }}
