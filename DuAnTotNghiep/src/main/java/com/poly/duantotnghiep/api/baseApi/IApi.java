package com.poly.duantotnghiep.api.baseApi;


import com.poly.duantotnghiep.dto.request.BaseRequestData;
import com.poly.duantotnghiep.dto.response.baseReponse.BaseResponseData;
import com.poly.duantotnghiep.utils.exception.ApplicationException;

public interface IApi {
    BaseResponseData excute(BaseRequestData request) throws ApplicationException;
}
