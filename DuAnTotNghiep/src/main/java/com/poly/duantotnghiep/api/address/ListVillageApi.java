package com.poly.duantotnghiep.api.address;


import com.poly.duantotnghiep.api.baseApi.IApi;
import com.poly.duantotnghiep.dto.request.BaseRequestData;
import com.poly.duantotnghiep.dto.request.address.DistrictRequest;
import com.poly.duantotnghiep.dto.request.address.VillageRequest;
import com.poly.duantotnghiep.dto.response.address.ListVillageResponse;
import com.poly.duantotnghiep.dto.response.baseReponse.BaseResponseData;
import com.poly.duantotnghiep.dto.response.baseReponse.IResponseData;
import com.poly.duantotnghiep.service.AddressService;
import com.poly.duantotnghiep.utils.exception.ApplicationException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component(value = "listVillage")
@Log4j2
public class ListVillageApi implements IApi {

	@Autowired
	AddressService addressService;

	@Override
	public BaseResponseData excute(BaseRequestData requestData) throws ApplicationException {
		BaseResponseData<IResponseData> baseResponseData = new BaseResponseData<>();
		try{
			VillageRequest villageRequest = (VillageRequest) requestData.getWsRequest();
			ListVillageResponse response = addressService.listVillage(requestData);
			baseResponseData.setWsResponse(response);
			baseResponseData.setErrorCode(0);
			baseResponseData.setMessage("SUCCESS");
			return baseResponseData;
		} catch (ApplicationException e){
			baseResponseData.setMessage(e.getLocalizedMessage());
			return baseResponseData;
		} finally {
			log.info("\n Request :" + requestData);
			log.info("\n Response :" + baseResponseData);
		}
	}
}
