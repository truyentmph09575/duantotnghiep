package com.poly.duantotnghiep.api.role;

import com.poly.duantotnghiep.api.baseApi.IApi;
import com.poly.duantotnghiep.constant.ApplicationCode;
import com.poly.duantotnghiep.dto.request.BaseRequestData;
import com.poly.duantotnghiep.dto.response.baseReponse.BaseResponseData;
import com.poly.duantotnghiep.dto.response.baseReponse.IResponseData;
import com.poly.duantotnghiep.dto.response.role.RoleRepone;
import com.poly.duantotnghiep.service.RoleService;
import com.poly.duantotnghiep.utils.exception.ApplicationException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

    @Component("addRole")
@Log4j2
public class AddRoleApi implements IApi {
    @Autowired
    RoleService roleService;
    @Override
    public BaseResponseData excute(BaseRequestData request) throws ApplicationException {
        BaseResponseData<IResponseData> baseResponseData = new BaseResponseData<>();
        try {
            RoleRepone listUserResponse = roleService.addRole(request);
            baseResponseData.setErrorCode(ApplicationCode.SUCCESS);
            baseResponseData.setMessage(ApplicationCode.getMessage(ApplicationCode.SUCCESS));
            baseResponseData.setWsResponse(listUserResponse);
        } catch (ApplicationException e) {
            baseResponseData.setErrorCode(e.getCode());
            baseResponseData.setMessage(e.getMessage());
        }
        return baseResponseData;
    }
}
