package com.poly.duantotnghiep.api.user;

import com.poly.duantotnghiep.api.baseApi.IApi;
import com.poly.duantotnghiep.constant.ApplicationCode;
import com.poly.duantotnghiep.dto.request.BaseRequestData;
import com.poly.duantotnghiep.dto.response.baseReponse.BaseResponseData;
import com.poly.duantotnghiep.dto.response.baseReponse.IResponseData;
import com.poly.duantotnghiep.dto.response.user.UserReponse;
import com.poly.duantotnghiep.service.UserService;
import com.poly.duantotnghiep.utils.exception.ApplicationException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.security.NoSuchAlgorithmException;

@Component("deleteUser")
@Log4j2
public class DeleteUserApi implements IApi {
    @Autowired
    UserService userService;
    @Override
    public BaseResponseData excute(BaseRequestData request) throws ApplicationException {
        BaseResponseData<IResponseData> baseResponseData = new BaseResponseData<>();
        try {

            UserReponse listUserResponse = userService.deleteUser(request);
            baseResponseData.setWsResponse(listUserResponse);
            baseResponseData.setErrorCode(ApplicationCode.SUCCESS);
            baseResponseData.setMessage(ApplicationCode.getMessage(ApplicationCode.SUCCESS));
        } catch (ApplicationException e) {
            baseResponseData.setErrorCode(e.getCode());
            baseResponseData.setMessage(e.getMessage());

        } catch (NoSuchAlgorithmException e){
            e.printStackTrace();

        }

        return baseResponseData;
    }
    }

