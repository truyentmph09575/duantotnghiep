package com.poly.duantotnghiep.constant;


import com.sun.istack.NotNull;
import org.springframework.util.StringUtils;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class ApplicationCode {
    private static final String LANGUAGE_DEFAULT = "vn";
    private static final ApplicationCode instance = new ApplicationCode();
    private static final Map<Integer, String> msg = new HashMap<>();

    private static final String ISO_8859 = "ISO-8859-1";
    private static final String UTF8 = "UTF-8";
    public static final int SUCCESS = 0;
    public static final int ERROR = 1;
    public static final int USER_REQUEST_EMPTY = 2;
    public static final int USER_EMPTY = 3;
    public static final int NOT_FOUND_USER = 6;
    public static final int TYPE_CAR_EXIST = 4;
    public static final int TYPE_CAR_NOT_FOUND = 5;
    public static final int UNAUTHORIZED = 401;


    public static final int UNIT_EXIST = 20;
    public static final int UNIT_NOT_FOUND = 21;
    public static final int UNIT_IS_NOT_NULL = 22;
    public static final int NOT_FOUND_CUSTOMER = 47;
    public static final int EMAIL_EXIST = 60;
    public static final int PHONE_EXIST = 61;
    public static final int PASSWORD_INVALID = 67;
    public static final int PHONE_OR_PASS_IS_NOT_NULL = 68;

//    @LOCCOC mã code 100-150
    public static final int COLOR_IS_NOT_NULL = 150;
    public static final int IMAGES_IS_NOT_NULL = 151;
    public static final int SIZE_IS_NOT_NULL = 152;

    public static final int NAME_IS_NOT_NULL = 153;
    public static final int NAME_PLATE_EXIST = 154;

    public static final int PLAN_EXIST = 23;
    public static final int PLAN_NOT_FOUND = 24;
    public static final int PLAN_IS_NOT_NULL = 25;
//    @Duy Lương mã code 150-200
    public static final int REASON_IS_NOT_NULL = 150;
    public static final int START_POINT_IS_NOT_NULL = 151;
    public static final int END_POINT_IS_NOT_NULL = 152;
    public static final int START_TIME_IS_NOT_NULL = 153;
    public static final int END_TIME_IS_NOT_NULL = 154;
    public static final int ABOUT_US_NOT_FOUND = 155;
    public static final int TITLE_IS_NOT_NULL = 156;
    public static final int CONTENT_IS_NOT_NULL = 157;
    public static final int SHORT_DESCRIPTION_IS_NOT_NULL = 158;
    public static final int EVENT_NOT_FOUND = 159;
    public static final int THUMBNAIL_IS_NOT_NULL = 160;
    // truyen 50-99
    public static  final int WRONG_OLD_PASSWORD=50;
    public static  final int USER_NAME_EXIST = 51;

    static {
        msg.put(SUCCESS, "SUCCESS");
        msg.put(ERROR, "ERROR");
        msg.put(USER_REQUEST_EMPTY, "USER_REQUEST_EMPTY");
        msg.put(USER_EMPTY, "USER_EMPTY");
        msg.put(TYPE_CAR_EXIST, "TYPE_CAR_EXIST");
        msg.put(TYPE_CAR_NOT_FOUND, "TYPE_CAR_NOT_FOUND");
        msg.put(UNAUTHORIZED, "UNAUTHORIZED");
        msg.put(UNIT_EXIST,"UNIT_EXIST");
        msg.put(UNIT_NOT_FOUND,"UNIT_NOT_FOUND");
        msg.put(UNIT_IS_NOT_NULL,"UNIT_IS_NOT_NULL");
        msg.put(EMAIL_EXIST,"EMAIL_EXIST");
        msg.put(PHONE_EXIST,"PHONE_EXIST");
        msg.put(NOT_FOUND_CUSTOMER, "Không tìm thấy thông tin khách hàng");
        msg.put(COLOR_IS_NOT_NULL, "COLOR_IS_NOT_NULL");
        msg.put(IMAGES_IS_NOT_NULL, "IMAGES_IS_NOT_NULL");
        msg.put(SIZE_IS_NOT_NULL, "SIZE_IS_NOT_NULL");
        msg.put(PHONE_OR_PASS_IS_NOT_NULL,"Số điện thoại và mật khẩu không được để trống");
        msg.put(NAME_IS_NOT_NULL, "NAME_IS_NOT_NULL");
        msg.put(NAME_PLATE_EXIST, "NAME_PLATE_EXIST");
        msg.put(NOT_FOUND_USER, "Tài khoản không tồn tại");
        msg.put(PASSWORD_INVALID,"Mật khẩu không chính xác");


        msg.put(PLAN_EXIST, "PLAN_EXIST");
        msg.put(PLAN_NOT_FOUND,"PLAN_NOT_FOUND");
        msg.put(PLAN_IS_NOT_NULL,"PLAN_IS_NOT_NULL");
        msg.put(REASON_IS_NOT_NULL, "REASON_IS_NOT_NULL");
        msg.put(START_POINT_IS_NOT_NULL, "START_POINT_IS_NOT_NULL");
        msg.put(END_POINT_IS_NOT_NULL, "END_POINT_IS_NOT_NULL");
        msg.put(START_TIME_IS_NOT_NULL, "START_TIME_IS_NOT_NULL");
        msg.put(END_TIME_IS_NOT_NULL, "END_TIME_IS_NOT_NULL");
        msg.put(ABOUT_US_NOT_FOUND, "ABOUT_US_NOT_FOUND");
        msg.put(TITLE_IS_NOT_NULL, "TITLE_IS_NOT_NULL");
        msg.put(CONTENT_IS_NOT_NULL, "CONTENT_IS_NOT_NULL");
        msg.put(SHORT_DESCRIPTION_IS_NOT_NULL, "SHORT_DESCRIPTION_IS_NOT_NULL");
        msg.put(THUMBNAIL_IS_NOT_NULL, "THUMBNAIL_IS_NOT_NULL");
        msg.put(EVENT_NOT_FOUND, "EVENT_NOT_FOUND");
        msg.put(WRONG_OLD_PASSWORD,"WRONG_OLD_PASSWORD");
        msg.put(USER_NAME_EXIST,"USER_NAME_EXIST");
    }

    public static ApplicationCode getInstance() {
        return instance;
    }


    public static ResourceBundle getBundle(String language) {
        ResourceBundle bundle = ResourceBundle.getBundle("language_" + language );
        return bundle;
    }

    public static String getProperty(int code, String language) {
        String text = msg.get(code);
        getBundle(language).keySet();
        return getBundle(language).getString(text);

    }

    public static String getMessage(int code) {
        return getMsg(code, LANGUAGE_DEFAULT);
    }

    public static String getMessage(int code, String language) {
        if (StringUtils.isEmpty(language)) {
            language = LANGUAGE_DEFAULT;
        }

        return getMsg(code, language);
    }

    @NotNull
    private static String getMsg(int code, String language) {
        if (msg.containsKey(code)) {
            String message = getProperty(code, language);
            try {
                String msg;
                if (code == 0) {
                    msg = new String(message.getBytes(UTF8), UTF8);
                } else {
                    msg = "[ERR_" + code + "] " + new String(message.getBytes(UTF8), UTF8);
                }
                return msg;
            } catch (UnsupportedEncodingException e) {
                return "";
            }
        }

        return "";
    }

    public static void main(String[] strs) {
        System.out.println("" + getMessage(0, "vn"));
    }
}
