package com.poly.duantotnghiep.constant;

public class Constant {
    public static final String SECRET_KEY = "f12jfh5h3h6k4k2k4jd3nx";
    public static final String KEY = "0000000000";
    public static final String FORMAT_DATE_TIME = "yyyy-MM-dd HH:mm:ss";
    // Config mail
    public static final String HOST_NAME = "smtp.gmail.com";
    public static final int SSL_PORT = 465; // Port for SSL
    public static final int TSL_PORT = 587; // Port for TLS/STARTTLS

    //    Trạng thái User
    public static int INACTIVE_USER = 0;
    public static int ACTIVE_USER = 1;
    public static int DELETE_USER = 2;

    //trang thai don hang
    public static int NEW_ORDER = 1;
    public static int CONFIRMED_ORDER = 2;
    public static int REFUSE_ORDER = 3;
    public static int RECEIVED_ORDER = 4;
    public static int CANCELED_ORDER = 5;
    public static int REFUND_ORDER = 6;
    //trang thai thanh toan
    public static int UNPAID = 0;
    public static int PAID = 1;
    //    Trạng thái about us
    public static int DELETE_ABOUT_US = 2;
    //    Trạng thái event
    public static int DELETE_EVENT = 2;

    //Status Room 0-Phòng trống, 1- Đã có người đặt, 2 - Không cho đặt
    public static int NOT_Room = 0;
    public static int ACTIVE_Room = 1;
    public static int INACTIVE_Room = 2;
    public static int DELETE_Room = 3;

    //trang thia unit
    public static final int DELETE_UNIT = 2;

    // trang thai thong bao
    public static final int DELETE_NOTIFICATION = 2;

    //    Trạng thái loại xe
    public static final int DELETE_TYPE_CAR = 2;
    public static final int ACTIVE_TYPE_CAR = 1;

    //    Trạng thái  xe
    public static final int ACTIVE_CAR = 1;
    //trang thai khach hang
    public static int ACTIVE_CUSTOMER = 1;
    public static int INACTIVE_CUSTOMER = 0;
    public static int NEW_CUSTOMER = 2;

    public static int ORDER_CUSTOMER = 1;
    public static int ORDER_PARTNER = 2;



}
