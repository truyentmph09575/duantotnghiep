package com.poly.duantotnghiep.repositoty;

import com.poly.duantotnghiep.model.Brand;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BrandRepository extends JpaRepository<Brand, Integer> {
        @Query(value = "select sc from Brand sc where (:textSearch is null or sc.name like %:textSearch%) ")
        List<Brand> findByBrandOrName(@Param("textSearch") String textSearch);

        @Query(value = "select c from Brand c where" +
                "(:name is null or c.name like %:name%)")
        List<Brand> getListBrand( @Param("name") String name, Pageable pageable);

        @Query(value = "select count(c) from Brand c where (:name is null or c.name like %:name%)")
        int countBrandbyName(@Param("name") String name);


        @Query(value = "select c from Brand c where c.brandId =?1")
        Brand findBrandId(int brandId);
        }
