package com.poly.duantotnghiep.repositoty;

import com.poly.duantotnghiep.model.Cart;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CartRepository extends JpaRepository<Cart ,Integer> {
    @Query(value = "select cart from  Cart cart where cart.userId = ?1 order by cart.createdDate desc")
    List<Cart> findCartByUser(int userId, Pageable pageable);
    @Query(value = "select count(cart) from  Cart cart where cart.userId = ?1 order by cart.createdDate desc")
    int countCartByUser(int userId);
    @Query(value = "select c from Cart c where c.userId =?1 and c.productId = ?2 ")
    Cart findCartByUserAndProduct(int userId, int productId);
}
