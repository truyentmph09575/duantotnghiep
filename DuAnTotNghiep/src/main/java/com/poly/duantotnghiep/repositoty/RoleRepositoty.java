package com.poly.duantotnghiep.repositoty;

import com.poly.duantotnghiep.model.Role;
import com.poly.duantotnghiep.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepositoty extends JpaRepository<Role, Integer> {
    @Query(value = "select us from Role us where us.name =:name")
    Role findTen (@Param("name") String name);

    @Query(value = "select c from Role c where c.idRole = ?1")
    Role findById(int idRole);
}
