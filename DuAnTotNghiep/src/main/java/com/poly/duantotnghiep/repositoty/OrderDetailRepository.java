package com.poly.duantotnghiep.repositoty;

import com.poly.duantotnghiep.model.OrderDetail;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
@Repository
public interface OrderDetailRepository extends JpaRepository<OrderDetail, Integer> {
    @Query(value = "select od from OrderDetail od where (:size is null or od.size like %:size%)" +
            "and (-1 in :ordersId or od.ordersId in :ordersId) and od.createdDate >= :createdDate " +
            "and od.modifiedDate <= :modifiedDate " +
            "and (-1 in : productId or od.productId in :productId)  ORDER BY od.createdDate DESC")
    List<OrderDetail> getListOrderDetail(@Param("size") String size, @Param("ordersId") int ordersId,
                                    @Param("createdDate") LocalDateTime createdDate, @Param("modifiedDate") LocalDateTime modifiedDate,
                                    @Param("productId") int productId, Pageable pageable);

    @Query(value = "select od from OrderDetail od where (:size is null or od.size like %:size%)" +
            "and (-1 in :ordersId or od.ordersId in :ordersId) and od.createdDate >= :createdDate " +
            "and od.modifiedDate <= :modifiedDate " +
            "and (-1 in : productId or od.productId in :productId)  ORDER BY od.createdDate DESC")
    int countOrderDetail(@Param("size") String size, @Param("ordersId") int ordersId,
                                         @Param("createdDate") LocalDateTime createdDate, @Param("modifiedDate") LocalDateTime modifiedDate,
                                         @Param("productId") int productId);


    @Query(value = "select count(c) from OrderDetail c where (:size is null or c.size like %:size%)")
    int countOrderDetailbyName(@Param("size") String size);



}

