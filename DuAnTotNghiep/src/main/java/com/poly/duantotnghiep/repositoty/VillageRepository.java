package com.poly.duantotnghiep.repositoty;


import com.poly.duantotnghiep.model.Village;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VillageRepository extends JpaRepository<Village, String> {
	List<Village> findByDistrictId(String districtId);
}
