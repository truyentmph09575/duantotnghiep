package com.poly.duantotnghiep.repositoty;


import com.poly.duantotnghiep.model.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository extends JpaRepository<City, String> {
}
