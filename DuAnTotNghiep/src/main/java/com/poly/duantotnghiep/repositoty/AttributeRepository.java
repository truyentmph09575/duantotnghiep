package com.poly.duantotnghiep.repositoty;

import com.poly.duantotnghiep.model.Attribute;
import com.poly.duantotnghiep.model.Brand;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface AttributeRepository extends JpaRepository<Attribute, Integer> {
    @Query(value = "select sc from Attribute sc where (:textSearch is null or sc.color like %:textSearch%) ")
    List<Attribute> findByAttributeOrName(@Param("textSearch") String textSearch);

    @Query(value = "select c from Attribute c where" +
            "(:color is null or c.color like %:color%)")
    List<Attribute> getListAttribute(@Param("color") String color, Pageable pageable);

    @Query(value = "select count(c) from Attribute c where (:color is null or c.color like %:color%)")
    int countAttributebyName(@Param("color") String color);


    @Query(value = "select c from Attribute c where c.attributeId =?1")
    Attribute findAttributeId(int attributeId);
}
