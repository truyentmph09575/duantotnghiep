package com.poly.duantotnghiep.repositoty;

import com.poly.duantotnghiep.model.Brand;
import com.poly.duantotnghiep.model.Category;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {
    @Query(value = "select sc from Category sc where (:textSearch is null or sc.name like %:textSearch%) ")
    List<Category> findByCategoryOrName(@Param("textSearch") String textSearch);

    @Query(value = "select c from Category c where" +
            "(:name is null or c.name like %:name%)")
    List<Category> getListCategory(@Param("name") String name, Pageable pageable);

    @Query(value = "select count(c) from Category c where (:name is null or c.name like %:name%)")
    int countCategorybyName(@Param("name") String name);


    @Query(value = "select c from Category c where c.categoryId =?1")
    Category findCategoryId(int categoryId);
}
