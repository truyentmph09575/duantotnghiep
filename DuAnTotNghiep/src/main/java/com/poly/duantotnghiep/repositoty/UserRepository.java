package com.poly.duantotnghiep.repositoty;

import com.poly.duantotnghiep.model.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    @Query(value = "Select count(u) from User u")
    int countUser();
    @Query(value = "select c from User c where c.userId = ?1")
    User findById(int id);


    @Query(value = "select u from User u where u.session = ?1 and u.token = ?2 and u.status = 1")
    User findBySessionAndToken(String session, String token);

    @Query(value = "select u from User u where u.username = ?1 and u.status = 1")
    User findByUsername(String username);
    @Query(value = "select us from User us where us.username= :username")
    User findUsername(@Param("username") String username);

//    @Query(value = "select u from User u where u.username = ?1")
//    User findUsername(String userName);

    @Query(value = "select u from User u where" +
            " (:status = -1 or u.status in :status) and (:username is null or u.username like %:username%) and u.status NOT IN (2)  and (:idRole = -1 or u.idRole in :idRole)")
    List<User> getListUser(@Param("status") int status, @Param("username") String username,@Param("idRole") int idRole,Pageable pageable);

    @Query(value = "select count(u) from User u where (:username is null or u.username like %:username%)" + " and (-1 in :status or u.status in :status)  and u.status NOT IN (2)  and (:idRole = -1 or u.idRole in :idRole)")
    int countUserStatusOrName(@Param("username") String username,@Param("status") int status,@Param("idRole") int idRole );

    @Query(value = "select u from User u where u.username =?1 and u.status <> 2")
    User findByNameByStatus(String name);





    @Transactional
    @Modifying
    @Query(value = "update User u set u.session = :session where u.username = :username")
    void updateSession(@Param("session") String session,
                       @Param("username") String username);
}
