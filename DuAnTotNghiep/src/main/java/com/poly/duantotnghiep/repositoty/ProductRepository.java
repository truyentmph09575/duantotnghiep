package com.poly.duantotnghiep.repositoty;

import com.poly.duantotnghiep.model.Attribute;
import com.poly.duantotnghiep.model.Product;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {
    @Query(value = "select sc from Product sc where (:textSearch is null or sc.name like %:textSearch%) ")
    List<Attribute> findByProductOrName(@Param("textSearch") String textSearch);

    @Query(value = "select c from Product c where" +
            "(:name is null or c.name like %:name%)")
    List<Product> getListProduct(@Param("name") String name, Pageable pageable);

    @Query(value = "select count(c) from Product c where (:name is null or c.name like %:name%)")
    int countProductbyName(@Param("name") String name);


    @Query(value = "select c from Product c where c.productId =?1")
    Product findProductId(int productId);
}
