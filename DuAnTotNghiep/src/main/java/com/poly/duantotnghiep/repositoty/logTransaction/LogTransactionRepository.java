package com.poly.duantotnghiep.repositoty.logTransaction;



import com.poly.duantotnghiep.model.logTransaction.LogTransactionBase;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogTransactionRepository extends CrudRepository<LogTransactionBase, Integer> {
}
