package com.poly.duantotnghiep.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "cart")
@EntityListeners(AuditingEntityListener.class)
public class Cart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cart_id")
    private int cartId;
    @Column(name = "created_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdDate;
    @Column(name = "modified_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime modifiedDate;
    @Column(name = "create_by")
    private String createBy;
    @Column(name = "modified_by")
    private String modifiedBy;
    @Column(name = "quantity")
    private  int quantity;

    @Column(name = "price")
    private  double price;
    @Column(name = "product_id")
    private  int productId;
    @Column(name = "user_id")
    private  int userId;
    @Column(name = "size")
    private  String size;
    @Column(name = "color")
    private  String color;
}
