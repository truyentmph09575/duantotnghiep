package com.poly.duantotnghiep.model;

import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Data
@Entity
@Table(name = "village")
@EntityListeners(AuditingEntityListener.class)
public class Village {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "village_id")
    private String villageId;
    @Column(name="name")
    private  String name;
    @Column(name = "type")
    private String type;
    @Column(name = "district_id")
    private  String districtId;
}
