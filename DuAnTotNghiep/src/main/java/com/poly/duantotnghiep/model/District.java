package com.poly.duantotnghiep.model;

import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Data
@Entity
@Table (name = "district")
@EntityListeners(AuditingEntityListener.class)
public class District {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "district_id")
    private String districtId;
    @Column(name="name")
    private  String name;
    @Column(name = "type")
    private String type;
    @Column(name = "city_id")
    private  String cityId;
}
