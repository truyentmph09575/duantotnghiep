package com.poly.duantotnghiep.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;


@Data
@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int userId;
    @Column(name = "created_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdDate;
    @Column(name = "modified_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime modifiedDate;
    @Column(name = "create_by")
    private String createBy;
    @Column(name = "modified_by")
    private String modifiedBy;
    @Column(name = "email")
    private  String email;
    @Column(name = "fullname")
    private  String fullName;
    @Column(name = "password")
    private String password;

    @Column(name = "status")
    private  int status;

    @Column(name = "username")
    private String username;

    @Column(name = "phone")
    private  String phone;
    @Column(name = "session")
    private String session;
    @Column(name = "token")
    private  String token;
    @Column(name = "id_role")
    private int idRole;
    @Column(name = "status_customer")
    private int statusCustomer;
    @Column(name = "city_id")
    private  String cityId;
    @Column(name = "district_id")
    private  String districtId;
    @Column(name = "village_id")
    private  String villageId;
    @Column(name = "sex")
    private int sex;

}
