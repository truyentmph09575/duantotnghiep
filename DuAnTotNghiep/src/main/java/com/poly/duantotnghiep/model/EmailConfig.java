package com.poly.duantotnghiep.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "email_config")
public class EmailConfig {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "action")
    private String action;
    @Column(name = "email")
    private String email;
    @Column(name = "password")
    private String password;
    @Column(name = "content")
    private String content;
    @Column(name = "subject")
    private String subject;
}
