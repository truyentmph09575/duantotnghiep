package com.poly.duantotnghiep.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "product")
@EntityListeners(AuditingEntityListener.class)

public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_id")
    private int productId;
    @Column(name = "created_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdDate;
    @Column(name = "modified_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime modifiedDate;
    @Column(name = "create_by")
    private String createBy;
    @Column(name = "modified_by")
    private String modifiedBy;
    @Column(name = "code")
    private  String code;
    @Column(name = "content")
    private String content;
    @Column(name = "images")
    private String images;
    @Column(name = "name")
    private  String name;
    @Column(name = "numberofsell")
    private  int numberofsell;
    @Column(name = "price")
    private  double price;
    @Column(name = "pricedescription")
    private  String pricedescription;
    @Column(name = "shortdescription")
    private  String shortdescription;
    @Column(name = "thumbnail")
    private  String thumbnail;
    @Column(name = "brand_id")
    private int brandId;
    @Column(name = "category_id")
    private int categoryId;
    @Column(name = "status_product")
    private int statusProduct;



}
