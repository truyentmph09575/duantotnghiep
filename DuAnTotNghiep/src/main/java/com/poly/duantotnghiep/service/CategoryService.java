package com.poly.duantotnghiep.service;

import com.poly.duantotnghiep.dto.request.BaseRequestData;


import com.poly.duantotnghiep.dto.response.category.CategoryResponse;
import com.poly.duantotnghiep.dto.response.category.ListCategoryResponse;

import com.poly.duantotnghiep.utils.exception.ApplicationException;



public interface CategoryService {
    CategoryResponse addCategory(BaseRequestData baseRequestData) throws ApplicationException;
    CategoryResponse detailCategory(BaseRequestData baseRequestData) throws ApplicationException;
    ListCategoryResponse getListCategory(BaseRequestData baseRequestData) throws ApplicationException;
    CategoryResponse updateCategory(BaseRequestData baseRequestData) throws ApplicationException;

}
