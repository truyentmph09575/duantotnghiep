package com.poly.duantotnghiep.service.impl;

import com.poly.duantotnghiep.constant.ApplicationCode;
import com.poly.duantotnghiep.dto.request.BaseRequestData;
import com.poly.duantotnghiep.dto.request.attribute.AttributeRequest;
import com.poly.duantotnghiep.dto.request.brand.BrandRequest;
import com.poly.duantotnghiep.dto.response.attribute.AttributeResponse;
import com.poly.duantotnghiep.dto.response.attribute.ListAttributeResponse;
import com.poly.duantotnghiep.dto.response.brand.BrandResponse;
import com.poly.duantotnghiep.dto.response.brand.ListBrandResponse;
import com.poly.duantotnghiep.model.Attribute;
import com.poly.duantotnghiep.model.Brand;
import com.poly.duantotnghiep.model.User;
import com.poly.duantotnghiep.repositoty.AttributeRepository;
import com.poly.duantotnghiep.repositoty.BrandRepository;
import com.poly.duantotnghiep.repositoty.UserRepository;
import com.poly.duantotnghiep.service.AttributeService;
import com.poly.duantotnghiep.service.BrandService;
import com.poly.duantotnghiep.utils.exception.ApplicationException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Component
public class BrandServiceImpl implements BrandService {
    @Autowired
    BrandRepository brandRepository;
    @Autowired
    UserRepository userRepository;

    @Override
    public BrandResponse addBrand(BaseRequestData baseRequestData) throws ApplicationException {
        BrandResponse brandResponse = new BrandResponse();

        BrandRequest brandRequest = (BrandRequest) baseRequestData.getWsRequest();
        try {
            User user = userRepository.findBySessionAndToken(baseRequestData.getSessionId(), baseRequestData.getToken());
            if (ObjectUtils.isEmpty(user)) {
                throw new ApplicationException(ApplicationCode.UNAUTHORIZED);
            }
            Brand brand = new Brand();

            BeanUtils.copyProperties(brandRequest, brand);
            brand.setCreateBy(user.getUsername());
            brand.setCreatedDate(LocalDateTime.now());
            brandRepository.save(brand);
            BeanUtils.copyProperties(brand, brandResponse);
        } catch (Exception e) {
            e.printStackTrace();
            throw  new ApplicationException(ApplicationCode.ERROR);

        }

        return brandResponse;
    }

    @Override
    public BrandResponse detailBrand(BaseRequestData baseRequestData) throws ApplicationException {
        BrandResponse brandResponse = new BrandResponse();
        BrandRequest brandRequest = (BrandRequest) baseRequestData.getWsRequest();
        try {
            User user = userRepository.findBySessionAndToken(baseRequestData.getSessionId(), baseRequestData.getToken());
            if (ObjectUtils.isEmpty(user)) {
                throw new ApplicationException(ApplicationCode.UNAUTHORIZED);
            }
            Brand brand=brandRepository.findById(brandRequest.getBrandId()).get();
            BeanUtils.copyProperties(brand, brandResponse);
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return brandResponse;
    }


    @Override
    public ListBrandResponse getListBrand(BaseRequestData baseRequestData) throws ApplicationException {
        ListBrandResponse listBrandResponse = new ListBrandResponse();
        BrandRequest brandRequest = (BrandRequest) baseRequestData.getWsRequest();
        try {
            User user = userRepository.findBySessionAndToken(baseRequestData.getSessionId(), baseRequestData.getToken());
            if (ObjectUtils.isEmpty(user)) {
                throw new ApplicationException(ApplicationCode.UNAUTHORIZED);
            }
            List<Brand> brandList =
                    brandRepository.getListBrand(brandRequest.getName(),
                            PageRequest.of(brandRequest.getPage(), brandRequest.getPageSize()));
            int totalItem = brandRepository.countBrandbyName(
                    brandRequest.getName());
            int totalPage = (int) Math.ceil((double) totalItem / (double) brandRequest.getPageSize());

            List<BrandResponse> brandResponses = new ArrayList<>();
            for (Brand brand : brandList) {
                BrandResponse brandResponse = new BrandResponse();
                BeanUtils.copyProperties(brand, brandResponse);
                brandResponses.add(brandResponse);
            }
            listBrandResponse.setTotalItem(totalItem);
            listBrandResponse.setTotalPage(totalPage);
            listBrandResponse.setBrandList(brandResponses);

        } catch (Exception e) {
            e.printStackTrace();

        }
        return listBrandResponse;
    }

        @Override
    public BrandResponse updateBrand(BaseRequestData baseRequestData) throws ApplicationException {
        BrandResponse brandResponse = new BrandResponse();
        BrandRequest brandRequest = (BrandRequest) baseRequestData.getWsRequest();
        Brand brand = brandRepository.findById(brandRequest.getBrandId()).get();

        try {
            User user = userRepository.findBySessionAndToken(baseRequestData.getSessionId(), baseRequestData.getToken());
            if (ObjectUtils.isEmpty(user)) {
                throw new ApplicationException(ApplicationCode.UNAUTHORIZED);
            }
            if (!ObjectUtils.isEmpty(brand)) {
                BeanUtils.copyProperties(brandRequest, brand);
                brand.setCreatedDate(brandRequest.getCreatedDate());
                brand.setModifiedDate(LocalDateTime.now());
                brand.setCreateBy(user.getUsername());
                brand.setModifiedBy(user.getUsername());
                brandRepository.save(brand);
                BeanUtils.copyProperties(brand, brandResponse);
            }
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return brandResponse;
    }
}
