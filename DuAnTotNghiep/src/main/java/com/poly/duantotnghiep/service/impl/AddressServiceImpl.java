package com.poly.duantotnghiep.service.impl;


import com.poly.duantotnghiep.dto.request.BaseRequestData;
import com.poly.duantotnghiep.dto.request.address.CityRequest;
import com.poly.duantotnghiep.dto.request.address.DistrictRequest;
import com.poly.duantotnghiep.dto.request.address.VillageRequest;
import com.poly.duantotnghiep.dto.response.address.*;

import com.poly.duantotnghiep.model.City;
import com.poly.duantotnghiep.model.District;
import com.poly.duantotnghiep.model.Village;
import com.poly.duantotnghiep.repositoty.CityRepository;
import com.poly.duantotnghiep.repositoty.DistrictRepository;
import com.poly.duantotnghiep.repositoty.VillageRepository;
import com.poly.duantotnghiep.service.AddressService;
import com.poly.duantotnghiep.utils.exception.ApplicationException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Component
@Service
public class AddressServiceImpl implements AddressService {

	@Autowired
	CityRepository cityRepository;
	@Autowired
	DistrictRepository districtRepository;
	@Autowired
	VillageRepository villageRepository;

	@Override
	public ListCityResponse listCity(BaseRequestData requestData) throws ApplicationException {
		ListCityResponse listCityResponse = new ListCityResponse();
		CityRequest cityRequest = (CityRequest) requestData.getWsRequest();
		List<CityResponse> cityResponses = new ArrayList<>();

		List<City> cityList = cityRepository.findAll();

		for (City city : cityList){
			CityResponse cityResponse = new CityResponse();
			BeanUtils.copyProperties(city, cityResponse);

			cityResponses.add(cityResponse);
		}
		listCityResponse.setCityResponseList(cityResponses);

		return listCityResponse;
	}

	@Override
	public CityResponse city(BaseRequestData requestData) throws ApplicationException{

		CityResponse cityResponse = new CityResponse();
		CityRequest cityRequest = (CityRequest) requestData.getWsRequest();
		City city = cityRepository.findById(cityRequest.getCityId()).get();
		BeanUtils.copyProperties(city, cityResponse);

		return cityResponse;
	}

	@Override
	public ListDistrictResponse listDistrict(BaseRequestData requestData) throws ApplicationException {

		ListDistrictResponse listDistrictResponse = new ListDistrictResponse();
		DistrictRequest districtRequest = (DistrictRequest) requestData.getWsRequest();
		List<DistrictResponse> districtResponses = new ArrayList<>();

		List<District> districtList = districtRepository.findByCityId(districtRequest.getCityId());

		for (District district : districtList){
			DistrictResponse districtResponse = new DistrictResponse();
			BeanUtils.copyProperties(district, districtResponse);

			districtResponses.add(districtResponse);
		}
		listDistrictResponse.setDistrictResponseList(districtResponses);

		return listDistrictResponse;
	}

	@Override
	public DistrictResponse district(BaseRequestData requestData) throws ApplicationException {

		DistrictResponse districtResponse = new DistrictResponse();
		DistrictRequest districtRequest = (DistrictRequest) requestData.getWsRequest();
		District district = districtRepository.findById(districtRequest.getDistrictId()).get();
		BeanUtils.copyProperties(district, districtResponse);

		return districtResponse;
	}

	@Override
	public VillageResponse village(BaseRequestData requestData) throws ApplicationException {

		VillageResponse villageResponse = new VillageResponse();
		VillageRequest villageRequest = (VillageRequest) requestData.getWsRequest();
		Village village = villageRepository.findById(villageRequest.getVillageId()).get();
		BeanUtils.copyProperties(village, villageResponse);

		return villageResponse;
	}

	@Override
	public ListVillageResponse listVillage(BaseRequestData requestData) throws ApplicationException {

		ListVillageResponse listVillageResponse = new ListVillageResponse();
		VillageRequest villageRequest = (VillageRequest) requestData.getWsRequest();
		List<VillageResponse> villageResponses = new ArrayList<>();

		List<Village> villageList = villageRepository.findByDistrictId(villageRequest.getDistrictId());

		for (Village village : villageList){
			VillageResponse villageResponse = new VillageResponse();
			BeanUtils.copyProperties(village, villageResponse);

			villageResponses.add(villageResponse);
		}
		listVillageResponse.setVillageResponseList(villageResponses);

		return listVillageResponse;
	}

}
