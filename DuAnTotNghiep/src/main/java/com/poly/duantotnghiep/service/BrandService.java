package com.poly.duantotnghiep.service;

import com.poly.duantotnghiep.dto.request.BaseRequestData;
import com.poly.duantotnghiep.dto.response.brand.BrandResponse;
import com.poly.duantotnghiep.dto.response.brand.ListBrandResponse;
import com.poly.duantotnghiep.utils.exception.ApplicationException;
import org.springframework.stereotype.Service;


public interface BrandService {
    BrandResponse addBrand(BaseRequestData baseRequestData) throws ApplicationException;
    BrandResponse detailBrand(BaseRequestData baseRequestData) throws ApplicationException;
    ListBrandResponse getListBrand(BaseRequestData baseRequestData) throws ApplicationException;
    BrandResponse updateBrand(BaseRequestData baseRequestData) throws ApplicationException;

}
