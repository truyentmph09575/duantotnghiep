package com.poly.duantotnghiep.service;

import com.poly.duantotnghiep.dto.request.BaseRequestData;
import com.poly.duantotnghiep.dto.response.attribute.AttributeResponse;
import com.poly.duantotnghiep.dto.response.attribute.ListAttributeResponse;
import com.poly.duantotnghiep.utils.exception.ApplicationException;
import org.springframework.stereotype.Service;


public interface AttributeService {
    AttributeResponse addAttribute(BaseRequestData baseRequestData) throws ApplicationException;
    AttributeResponse detailAttribute(BaseRequestData baseRequestData) throws ApplicationException;
    ListAttributeResponse getListAttribute(BaseRequestData baseRequestData) throws ApplicationException;
    AttributeResponse updateAttribute(BaseRequestData baseRequestData) throws ApplicationException;


}
