package com.poly.duantotnghiep.service;

import com.poly.duantotnghiep.dto.request.BaseRequestData;
import com.poly.duantotnghiep.dto.response.role.RoleRepone;
import com.poly.duantotnghiep.utils.exception.ApplicationException;



public interface RoleService {
    RoleRepone addRole (BaseRequestData baseRequestData) throws ApplicationException;
}
