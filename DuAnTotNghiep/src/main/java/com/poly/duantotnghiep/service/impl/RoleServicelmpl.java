package com.poly.duantotnghiep.service.impl;

import com.poly.duantotnghiep.constant.ApplicationCode;
import com.poly.duantotnghiep.dto.request.BaseRequestData;
import com.poly.duantotnghiep.dto.request.role.RoleRequest;
import com.poly.duantotnghiep.dto.response.role.RoleRepone;
import com.poly.duantotnghiep.model.Role;
import com.poly.duantotnghiep.repositoty.RoleRepositoty;
import com.poly.duantotnghiep.service.RoleService;
import com.poly.duantotnghiep.utils.exception.ApplicationException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
public class RoleServicelmpl implements RoleService {
    @Autowired
    RoleRepositoty roleRepositoty;
    @Override
    public RoleRepone addRole(BaseRequestData baseRequestData) throws ApplicationException {
        RoleRepone roleRepone = new RoleRepone();
        RoleRequest roleRequest = (RoleRequest) baseRequestData.getWsRequest();

        try {
            Role role = new Role();
            if (StringUtils.isEmpty(roleRequest.getName())) {
                throw new ApplicationException(ApplicationCode.NAME_IS_NOT_NULL);
            }
            if (!ObjectUtils.isEmpty(roleRepositoty.findTen(role.getName()))) {
                throw new ApplicationException(ApplicationCode.NAME_PLATE_EXIST);
            }
            BeanUtils.copyProperties(roleRequest,role);
            roleRepositoty.save(role);
            BeanUtils.copyProperties(role,roleRepone);
        } catch (ApplicationException e) {
            throw e;
        }
        return roleRepone;
    }
}
