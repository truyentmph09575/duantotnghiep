package com.poly.duantotnghiep.service;

import com.poly.duantotnghiep.dto.request.BaseRequestData;
import com.poly.duantotnghiep.dto.request.user.UserRequest;
import com.poly.duantotnghiep.dto.response.user.ListUserRepone;
import com.poly.duantotnghiep.dto.response.user.UserReponse;
import com.poly.duantotnghiep.utils.exception.ApplicationException;

import java.security.NoSuchAlgorithmException;

public interface UserService {
    UserReponse login(UserRequest userRequest) throws ApplicationException;
    UserReponse addUser(BaseRequestData baseRequestData) throws ApplicationException, NoSuchAlgorithmException;
    UserReponse updateUser(BaseRequestData baseRequestData) throws ApplicationException, NoSuchAlgorithmException;
    UserReponse deleteUser(BaseRequestData baseRequestData) throws ApplicationException, NoSuchAlgorithmException;
    UserReponse detailUser(BaseRequestData baseRequestData) throws ApplicationException;
    ListUserRepone getListUser(BaseRequestData baseRequestData) throws ApplicationException;
    boolean selfChangePassword(BaseRequestData baseRequestData) throws ApplicationException;
}
