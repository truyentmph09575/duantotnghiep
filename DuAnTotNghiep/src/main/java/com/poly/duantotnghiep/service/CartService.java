package com.poly.duantotnghiep.service;


import com.poly.duantotnghiep.dto.request.BaseRequestData;
import com.poly.duantotnghiep.dto.response.baseReponse.BaseResponseData;
import com.poly.duantotnghiep.dto.response.cart.CartRespone;
import com.poly.duantotnghiep.dto.response.cart.ListCartRespone;
import com.poly.duantotnghiep.utils.exception.ApplicationException;

public interface CartService {
    CartRespone addCart(BaseRequestData baseRequestData) throws ApplicationException;
    CartRespone deleteCart(BaseRequestData baseRequestData) throws ApplicationException;
    ListCartRespone getListCart(BaseRequestData baseRequestData) throws ApplicationException;
}
