package com.poly.duantotnghiep.service.impl;

import com.poly.duantotnghiep.constant.ApplicationCode;
import com.poly.duantotnghiep.constant.Constant;
import com.poly.duantotnghiep.dto.request.BaseRequestData;
import com.poly.duantotnghiep.dto.request.user.UserRequest;
import com.poly.duantotnghiep.dto.response.user.ListUserRepone;
import com.poly.duantotnghiep.dto.response.user.UserReponse;
import com.poly.duantotnghiep.model.Role;
import com.poly.duantotnghiep.model.*;
import com.poly.duantotnghiep.model.User;
import com.poly.duantotnghiep.repositoty.RoleRepositoty;
import com.poly.duantotnghiep.repositoty.UserRepository;
import com.poly.duantotnghiep.service.RoleService;
import com.poly.duantotnghiep.service.UserService;
import com.poly.duantotnghiep.utils.GenCodeUtils;
import com.poly.duantotnghiep.utils.PasswordEncryption;
import com.poly.duantotnghiep.utils.exception.ApplicationException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
@Log4j2
public class UserServicelmpl implements UserService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepositoty roleRepositoty;

    @Override
    public UserReponse login(UserRequest userRequest) throws ApplicationException {
        UserReponse userRespone = null;
        try {
            User user = userRepository.findUsername(userRequest.getUsername());
            if (ObjectUtils.isEmpty(user)) {
                throw new ApplicationException(ApplicationCode.USER_EMPTY);
            }

            boolean checkUser = PasswordEncryption.bCryptPasswordEncoder(userRequest.getPassword(), user.getPassword());
            if (checkUser) {
                userRespone = new UserReponse();
                BeanUtils.copyProperties(user, userRespone);
                String tokenData = GenCodeUtils.encrypt(user.getUsername() + "_" + user.getUsername() + "_" + user.getUserId(), Constant.KEY, Constant.SECRET_KEY);
                userRespone.setToken(tokenData);
                user.setToken(tokenData);
                BeanUtils.copyProperties(user, userRespone);

                userRepository.save(user);
            } else {
                throw new ApplicationException(ApplicationCode.WRONG_OLD_PASSWORD);
            }
        } catch (ApplicationException e) {
            e.printStackTrace();
            throw e;
        }
        return userRespone;

    }

    @Override
    public UserReponse addUser(BaseRequestData baseRequestData) throws ApplicationException, NoSuchAlgorithmException {
        UserRequest userRequest = (UserRequest) baseRequestData.getWsRequest();
        UserReponse userResponse = new UserReponse();
        try {
            User user = new User();
//            if (ObjectUtils.isEmpty(userRepository.findBySessionAndToken(baseRequestData.getSessionId(), baseRequestData.getToken()))) {
//               throw new ApplicationException(ApplicationCode.UNAUTHORIZED);
//            }

            if (ObjectUtils.isEmpty(userRequest)) {
                throw new ApplicationException(ApplicationCode.USER_REQUEST_EMPTY);
            }
            String username = userRequest.getUsername();
            if (!username.matches("[a-zA-Z0-9]{4,}")) {
                throw new ApplicationException(ApplicationCode.USER_EMPTY);
            }
            if (!ObjectUtils.isEmpty(userRepository.findUsername(userRequest.getUsername()))) {
                throw new ApplicationException(ApplicationCode.USER_NAME_EXIST);
            }
            BeanUtils.copyProperties(userRequest, user);
            user.setPassword(PasswordEncryption.encryteBCryptPassword("12345678"));


//           if (ObjectUtils.isEmpty(user)) {
//               throw new ApplicationException(ApplicationCode.USER_EMPTY);
//            }
            user.setCreatedDate(LocalDateTime.now());
            user.setModifiedDate(LocalDateTime.now());
            user.setCreateBy(user.getUsername());
            userRepository.save(user);
            BeanUtils.copyProperties(user, userResponse);


        } catch (ApplicationException e) {
            e.printStackTrace();
            throw e;
        }
        return userResponse;
    }

    @Override
    public UserReponse updateUser(BaseRequestData baseRequestData) throws ApplicationException, NoSuchAlgorithmException {
        UserRequest userRequest = (UserRequest) baseRequestData.getWsRequest();
        UserReponse userResponse = new UserReponse();
        try {
//            if (ObjectUtils.isEmpty(userRepository.findBySessionAndToken(baseRequestData.getSessionId(), baseRequestData.getToken()))) {
//                throw new ApplicationException(ApplicationCode.UNAUTHORIZED);
//            }
            User user = userRepository.findById(userRequest.getUserId());
            if (ObjectUtils.isEmpty(userRequest)) {
                throw new ApplicationException(ApplicationCode.USER_REQUEST_EMPTY);
            }
            if (StringUtils.isEmpty(userRequest.getUsername())) {
                throw new ApplicationException(ApplicationCode.USER_EMPTY);
            }
            if (!ObjectUtils.isEmpty(userRepository.findUsername(userRequest.getUsername()))) {
                throw new ApplicationException(ApplicationCode.USER_NAME_EXIST);
            }

            BeanUtils.copyProperties(userRequest, user);
            user.setUsername(userRequest.getUsername());
            user.setModifiedBy(userRequest.getUsername());
            user.setModifiedDate(LocalDateTime.now());
            user.setStatus(userRequest.getStatus());
            user.setIdRole(userRequest.getIdRole());

            userRepository.save(user);
            if (ObjectUtils.isEmpty(user)) {
                throw new ApplicationException(ApplicationCode.USER_EMPTY);
            }
            BeanUtils.copyProperties(user, userResponse);

        } catch (ApplicationException e) {
            e.getLocalizedMessage();
            throw e;
        }


        return userResponse;
    }

    @Override
    public UserReponse deleteUser(BaseRequestData baseRequestData) throws ApplicationException, NoSuchAlgorithmException {
        UserReponse userResponse = new UserReponse();
        UserRequest userRequest = (UserRequest) baseRequestData.getWsRequest();
        try {
            User user = userRepository.findById(userRequest.getUserId());
            if (ObjectUtils.isEmpty(userRequest)) {
                throw new ApplicationException(ApplicationCode.USER_REQUEST_EMPTY);
            }
            user.setStatus(Constant.DELETE_USER);
            userRepository.save(user);
            BeanUtils.copyProperties(user, userResponse);
        } catch (ApplicationException e) {
            e.getLocalizedMessage();
            throw e;
        }
        return userResponse;
    }

    @Override
    public UserReponse detailUser(BaseRequestData baseRequestData) throws ApplicationException {
        UserReponse userResponse = new UserReponse();
        UserRequest userRequest = (UserRequest) baseRequestData.getWsRequest();
        try {
            User user = userRepository.findById(userRequest.getUserId());
            if (ObjectUtils.isEmpty(userRequest)) {
                throw new ApplicationException(ApplicationCode.USER_REQUEST_EMPTY);
            }
            BeanUtils.copyProperties(user, userResponse);
        } catch (ApplicationException e) {
            e.getLocalizedMessage();
            throw e;
        }
        return userResponse;
    }

    @Override
    public ListUserRepone getListUser(BaseRequestData baseRequestData) throws ApplicationException {
//        HashMap<String, Role> rolesHashMap = new HashMap<>();
//        List<Role> roleList = roleRepositoty.findAll();
        //Lấy danh sách role đẩy vào map
//        for (Role role : roleList) {
//            rolesHashMap.put(String.valueOf(role.getIdRole()), role);
//        }
        ListUserRepone listUserResponse = new ListUserRepone();
        UserRequest userRequest = (UserRequest) baseRequestData.getWsRequest();
        try {
            if (ObjectUtils.isEmpty(userRepository.findBySessionAndToken(baseRequestData.getSessionId(), baseRequestData.getToken()))) {
                throw new ApplicationException(ApplicationCode.UNAUTHORIZED);
            }
            List<User> userList = userRepository.getListUser(userRequest.getStatus(), userRequest.getUsername(), userRequest.getIdRole(),
                    PageRequest.of(userRequest.getPage(), userRequest.getPageSize()));
            int totalItem = userRepository.countUserStatusOrName(
                    userRequest.getUsername(), userRequest.getStatus(), userRequest.getIdRole());
            int totalPage = (int) Math.ceil((double) totalItem / (double) userRequest.getPageSize());

            List<UserReponse> userResponse = new ArrayList<>();
            for (User user : userList) {
                UserReponse userResponses = new UserReponse();
                BeanUtils.copyProperties(user, userResponses);

                Role role = roleRepositoty.findById(user.getIdRole());
//                String nameRole = rolesHashMap.get(String.valueOf(user.getIdRole())).getName();
                userResponses.setNameRole(role.getName());
                userResponse.add(userResponses);
            }
            listUserResponse.setTotalItem(totalItem);
            listUserResponse.setTotalPage(totalPage);
            listUserResponse.setUserListResponse(userResponse);

        } catch (ApplicationException e) {
            e.getLocalizedMessage();
            throw e;
        }
        return listUserResponse;
    }



    @Override
    public boolean selfChangePassword(BaseRequestData baseRequestData) throws ApplicationException {
        boolean success = false;
        UserRequest userRequest = (UserRequest) baseRequestData.getWsRequest();
        try {
            User user = userRepository.findBySessionAndToken(baseRequestData.getSessionId(), baseRequestData.getToken());
            if (ObjectUtils.isEmpty(user) || user.getUserId() != userRequest.getUserId()) {
                throw new ApplicationException(ApplicationCode.UNAUTHORIZED);
            }
            if (PasswordEncryption.bCryptPasswordEncoder(userRequest.getPassword(), user.getPassword())) {
                user.setPassword(PasswordEncryption.encryteBCryptPassword(userRequest.getNewPass()));
                userRepository.save(user);
                success = true;
            } else {
                throw new ApplicationException(ApplicationCode.ERROR);
            }
        } catch (ApplicationException e) {
            throw e;
        }
        return success;
    }
}
