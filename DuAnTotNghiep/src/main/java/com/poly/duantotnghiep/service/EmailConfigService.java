package com.poly.duantotnghiep.service;


import com.poly.duantotnghiep.constant.Constant;
import com.poly.duantotnghiep.model.EmailConfig;
import com.poly.duantotnghiep.repositoty.EmailConfigRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.List;
import java.util.Properties;

@Component
public class EmailConfigService {

    @Autowired
    EmailConfigRepository emailConfigRepository;

    public void senMail(String action, String toMail, BodyPart bodyPart) {
        EmailConfig emailConfig = emailConfigRepository.findByAction(action);
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", Constant.HOST_NAME);
//        props.put("mail.smtp.port", Constant.SSL_PORT);
        props.put("mail.smtp.port", Constant.TSL_PORT);

        Session session = Session.getInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(emailConfig.getEmail(), emailConfig.getPassword());
            }
        });
        // compose message
        try {
            MimeMessage message = new MimeMessage(session);
            message.setRecipients(Message.RecipientType.CC, toMail);
            message.setSubject(emailConfig.getSubject());

            //thiết lập nội dụng mail
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(bodyPart);
            message.setContent(multipart);
            // send message
            Transport.send(message);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    public  void senMailMulti(String action, List<String> toMail, BodyPart bodyPart) {
        EmailConfig emailConfig = emailConfigRepository.findByAction(action);
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", Constant.HOST_NAME);
//        props.put("mail.smtp.port", Constant.SSL_PORT);
        props.put("mail.smtp.port", Constant.TSL_PORT);

        Session session = Session.getInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(emailConfig.getEmail(), emailConfig.getPassword());
            }
        });
        // compose message
        try {
            MimeMessage message = new MimeMessage(session);
            message.setRecipients(Message.RecipientType.TO, getAddress(toMail));
            message.setSubject(emailConfig.getSubject());

            //thiết lập nội dụng mail
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(bodyPart);
            message.setContent(multipart);
            // send message
            Transport.send(message);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    public  InternetAddress[] getAddress(List<String> mailAddress) throws AddressException {
        InternetAddress[] addresses = new InternetAddress[mailAddress.size()];
        for (int i = 0; i < mailAddress.size(); i++) {
            InternetAddress internetAddress = new InternetAddress(mailAddress.get(i));
            addresses[i] = internetAddress;
//            InternetAddress[] internetAddress = InternetAddress.parse(mailAddress.get(i));
        }
        return addresses;
    }

}
