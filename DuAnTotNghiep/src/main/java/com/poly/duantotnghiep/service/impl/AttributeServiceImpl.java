package com.poly.duantotnghiep.service.impl;

import com.poly.duantotnghiep.constant.ApplicationCode;
import com.poly.duantotnghiep.dto.request.BaseRequestData;
import com.poly.duantotnghiep.dto.request.attribute.AttributeRequest;
import com.poly.duantotnghiep.dto.request.brand.BrandRequest;
import com.poly.duantotnghiep.dto.request.product.ProductRequest;
import com.poly.duantotnghiep.dto.response.attribute.AttributeResponse;
import com.poly.duantotnghiep.dto.response.attribute.ListAttributeResponse;
import com.poly.duantotnghiep.dto.response.brand.BrandResponse;
import com.poly.duantotnghiep.dto.response.brand.ListBrandResponse;
import com.poly.duantotnghiep.dto.response.product.ProductResponse;
import com.poly.duantotnghiep.model.Attribute;
import com.poly.duantotnghiep.model.Brand;
import com.poly.duantotnghiep.model.Product;
import com.poly.duantotnghiep.model.User;
import com.poly.duantotnghiep.repositoty.AttributeRepository;
import com.poly.duantotnghiep.repositoty.ProductRepository;
import com.poly.duantotnghiep.repositoty.UserRepository;
import com.poly.duantotnghiep.service.AttributeService;
import com.poly.duantotnghiep.utils.exception.ApplicationException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Service
@Component
public class AttributeServiceImpl implements AttributeService {
    @Autowired
    AttributeRepository attributeRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ProductRepository productRepository;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public AttributeResponse addAttribute(BaseRequestData baseRequestData) throws ApplicationException {
        AttributeResponse attributeResponse = new AttributeResponse();
        AttributeRequest attributeRequest = (AttributeRequest) baseRequestData.getWsRequest();
        try {
            User user = userRepository.findBySessionAndToken(baseRequestData.getSessionId(), baseRequestData.getToken());
            if (ObjectUtils.isEmpty(user)) {
                throw new ApplicationException(ApplicationCode.UNAUTHORIZED);
            }
            if (StringUtils.isEmpty(attributeRequest.getColor())) {
                throw new ApplicationException(ApplicationCode.COLOR_IS_NOT_NULL);
            }
            if (StringUtils.isEmpty(attributeRequest.getImages())) {
                throw new ApplicationException(ApplicationCode.IMAGES_IS_NOT_NULL);
            }
            if (StringUtils.isEmpty(attributeRequest.getSize())) {
                throw new ApplicationException(ApplicationCode.SIZE_IS_NOT_NULL);
            }

            Attribute attribute = new Attribute();

            BeanUtils.copyProperties(attributeRequest, attribute);
            attribute.setCreatedDate(LocalDateTime.now());
            attribute.setCreateBy(user.getUsername());

            attributeRepository.save(attribute);
            BeanUtils.copyProperties(attribute, attributeResponse);
        } catch (Exception e) {
            e.printStackTrace();
            throw  new ApplicationException(ApplicationCode.ERROR);

        }



        return attributeResponse;
    }


    @Override
    public AttributeResponse detailAttribute(BaseRequestData baseRequestData) throws ApplicationException {
            AttributeResponse attributeResponse = new AttributeResponse();
            AttributeRequest attributeRequest = (AttributeRequest) baseRequestData.getWsRequest();
            try {
                if (ObjectUtils.isEmpty(userRepository.findBySessionAndToken(baseRequestData.getSessionId(), baseRequestData.getToken()))) {
                    throw new ApplicationException(ApplicationCode.UNAUTHORIZED);
                }
                Attribute attribute=attributeRepository.findById(attributeRequest.getAttributeId()).get();
                BeanUtils.copyProperties(attribute, attributeResponse);
            } catch (Exception e) {
                e.getLocalizedMessage();
            }
            return attributeResponse;
        }


    @Override
    public ListAttributeResponse getListAttribute(BaseRequestData baseRequestData) throws ApplicationException {

        ListAttributeResponse listAttributeResponse = new ListAttributeResponse();
        AttributeRequest attributeRequest = (AttributeRequest) baseRequestData.getWsRequest();
        try {
            if (ObjectUtils.isEmpty(userRepository.findBySessionAndToken(baseRequestData.getSessionId(), baseRequestData.getToken()))) {
                throw new ApplicationException(ApplicationCode.UNAUTHORIZED);
            }
            List<Attribute> attributeList =
                    attributeRepository.getListAttribute(attributeRequest.getColor(),
                            PageRequest.of(attributeRequest.getPage(), attributeRequest.getPageSize()));
            int totalItem = attributeRepository.countAttributebyName(
                    attributeRequest.getColor());
            int totalPage = (int) Math.ceil((double) totalItem / (double) attributeRequest.getPageSize());

            List<AttributeResponse> attributeResponses = new ArrayList<>();
            for (Attribute attribute : attributeList) {
                AttributeResponse attributeResponse = new AttributeResponse();
                BeanUtils.copyProperties(attribute, attributeResponse);
                attributeResponses.add(attributeResponse);
            }
            listAttributeResponse.setTotalItem(totalItem);
            listAttributeResponse.setTotalPage(totalPage);
            listAttributeResponse.setAttributeList(attributeResponses);

        } catch (Exception e) {
            e.printStackTrace();

        }
        return listAttributeResponse;
    }


    @Override
    public AttributeResponse updateAttribute(BaseRequestData baseRequestData) throws ApplicationException {
            AttributeResponse attributeResponse = new AttributeResponse();
            AttributeRequest attributeRequest = (AttributeRequest) baseRequestData.getWsRequest();
            Attribute attribute = attributeRepository.findById(attributeRequest.getAttributeId()).get();

            try {
                User user = userRepository.findBySessionAndToken(baseRequestData.getSessionId(), baseRequestData.getToken());
                if (ObjectUtils.isEmpty(user)) {
                    throw new ApplicationException(ApplicationCode.UNAUTHORIZED);
                }
                if (StringUtils.isEmpty(attributeRequest.getColor())) {
                    throw new ApplicationException(ApplicationCode.COLOR_IS_NOT_NULL);
                }
                if (StringUtils.isEmpty(attributeRequest.getImages())) {
                    throw new ApplicationException(ApplicationCode.IMAGES_IS_NOT_NULL);
                }
                if (StringUtils.isEmpty(attributeRequest.getSize())) {
                    throw new ApplicationException(ApplicationCode.SIZE_IS_NOT_NULL);
                }
                if (!ObjectUtils.isEmpty(attribute)) {
                    BeanUtils.copyProperties(attributeRequest, attribute);
                    attribute.setCreatedDate(attributeRequest.getCreatedDate());
                    attribute.setModifiedDate(LocalDateTime.now());
                    attribute.setCreateBy(user.getUsername());
                    attribute.setModifiedBy(user.getUsername());
                    attribute.setColor(attributeRequest.getColor());
                    attribute.setImages(attributeRequest.getImages());
                    attribute.setSize(attributeRequest.getSize());
                    attributeRepository.save(attribute);
                    BeanUtils.copyProperties(attribute, attributeResponse);
                }
            } catch (Exception e) {
                e.getLocalizedMessage();
            }
            return attributeResponse;
        }


    }




