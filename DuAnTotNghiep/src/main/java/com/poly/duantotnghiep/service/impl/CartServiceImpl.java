package com.poly.duantotnghiep.service.impl;


import com.poly.duantotnghiep.constant.ApplicationCode;
import com.poly.duantotnghiep.dto.request.BaseRequestData;
import com.poly.duantotnghiep.dto.request.cart.CartRequest;
import com.poly.duantotnghiep.dto.request.category.CategoryRequest;
import com.poly.duantotnghiep.dto.response.baseReponse.BaseResponseData;
import com.poly.duantotnghiep.dto.response.cart.CartRespone;
import com.poly.duantotnghiep.dto.response.cart.ListCartRespone;
import com.poly.duantotnghiep.model.Cart;
import com.poly.duantotnghiep.model.Product;
import com.poly.duantotnghiep.model.User;
import com.poly.duantotnghiep.repositoty.CartRepository;
import com.poly.duantotnghiep.repositoty.ProductRepository;
import com.poly.duantotnghiep.repositoty.UserRepository;
import com.poly.duantotnghiep.service.CartService;
import com.poly.duantotnghiep.utils.StringUtils;
import com.poly.duantotnghiep.utils.exception.ApplicationException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
@Service
public class CartServiceImpl implements CartService {
    @Autowired
    CartRepository cartRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    ProductRepository productRepository;
    @Override
    public CartRespone addCart(BaseRequestData baseRequestData) throws ApplicationException {
        CartRequest cartRequest = (CartRequest) baseRequestData.getWsRequest();
        CartRespone cartRespone = new CartRespone();

        try {
            User user = userRepository.findBySessionAndToken(baseRequestData.getSessionId(), baseRequestData.getToken());
            if (ObjectUtils.isEmpty(user)) {
                throw new ApplicationException(ApplicationCode.UNAUTHORIZED);
            }
            Cart cart = cartRepository.findCartByUserAndProduct(cartRequest.getUserId(),cartRequest.getProductId());
            if (ObjectUtils.isEmpty(cart)) {
                cart = new Cart();
                BeanUtils.copyProperties(cartRequest, cart);
                Product product =productRepository.findProductId(cartRequest.getProductId());
                cart.setCreatedDate(LocalDateTime.now());
                cart.setModifiedDate(LocalDateTime.now());
                cart.setCreateBy(user.getUsername());
                cart.setProductId(cartRequest.getProductId());
                cart.setPrice(product.getPrice());
                cartRepository.save(cart);
                BeanUtils.copyProperties(cart, cartRespone);
            }
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return cartRespone;
    }


    @Override
    public CartRespone deleteCart(BaseRequestData baseRequestData) throws ApplicationException {
        CartRequest cartRequest = (CartRequest) baseRequestData.getWsRequest();
        CartRespone cartRespone = new CartRespone();
        try {
            User user = userRepository.findBySessionAndToken(baseRequestData.getSessionId(), baseRequestData.getToken());
            if (ObjectUtils.isEmpty(user)) {
                throw new ApplicationException(ApplicationCode.UNAUTHORIZED);
            }
            Cart cart = cartRepository.findById(cartRequest.getCartId()).get();
            if (!ObjectUtils.isEmpty(cart)){
                BeanUtils.copyProperties(cart, cartRespone);
                cartRepository.delete(cart);
            }
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return cartRespone;
    }

    @Override
    public ListCartRespone getListCart(BaseRequestData baseRequestData) throws ApplicationException {
        CartRequest cartRequest = (CartRequest) baseRequestData.getWsRequest();
        ListCartRespone listCartRespone = new ListCartRespone();
        try {


            List<Cart> cartList = cartRepository.findCartByUser(cartRequest.getUserId(),
                    PageRequest.of(cartRequest.getPage(), cartRequest.getPageSize()));
            int totalItem = cartRepository.countCartByUser(cartRequest.getUserId());
            int totalPage =(int) Math.ceil((double) totalItem/(double) cartRequest.getPageSize());
            List<CartRespone> cartResponeList = new ArrayList<>();
            double totalPrice = 0;
            double pricce=0;
            for (Cart cart : cartList){
                CartRespone cartRespone = new CartRespone();
                BeanUtils.copyProperties(cart, cartRespone);
                Product prod = productRepository.findProductId(cart.getProductId());


                pricce = cart.getQuantity() * prod.getPrice();

                totalPrice += pricce;
                cartResponeList.add(cartRespone);
            }

            listCartRespone.setTotalItem(totalItem);
            listCartRespone.setTotalPage(totalPage);
            listCartRespone.setTotalPrice(totalPrice);
            listCartRespone.setCartResponeList(cartResponeList);
        } catch (Exception e){
            e.getLocalizedMessage();
        }
        return listCartRespone;
    }
    }

