package com.poly.duantotnghiep.service;


import com.poly.duantotnghiep.dto.request.BaseRequestData;
import com.poly.duantotnghiep.dto.response.address.*;
import com.poly.duantotnghiep.dto.response.baseReponse.BaseResponseData;
import com.poly.duantotnghiep.utils.exception.ApplicationException;
import org.springframework.stereotype.Service;

@Service
public interface AddressService {

	ListCityResponse listCity(BaseRequestData requestData ) throws ApplicationException;

	ListDistrictResponse listDistrict(BaseRequestData requestData) throws ApplicationException;

	ListVillageResponse listVillage(BaseRequestData requestData) throws ApplicationException;

	CityResponse city(BaseRequestData requestData) throws ApplicationException;

	DistrictResponse district(BaseRequestData requestData) throws ApplicationException;

	VillageResponse village(BaseRequestData requestData) throws ApplicationException;

}
