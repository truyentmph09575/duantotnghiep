package com.poly.duantotnghiep.service.impl;

import com.poly.duantotnghiep.constant.ApplicationCode;
import com.poly.duantotnghiep.dto.request.BaseRequestData;
import com.poly.duantotnghiep.dto.request.brand.BrandRequest;
import com.poly.duantotnghiep.dto.request.product.ProductRequest;
import com.poly.duantotnghiep.dto.response.brand.BrandResponse;
import com.poly.duantotnghiep.dto.response.brand.ListBrandResponse;
import com.poly.duantotnghiep.dto.response.product.ListProductResponse;
import com.poly.duantotnghiep.dto.response.product.ProductResponse;
import com.poly.duantotnghiep.model.Brand;
import com.poly.duantotnghiep.model.Product;
import com.poly.duantotnghiep.model.User;
import com.poly.duantotnghiep.repositoty.BrandRepository;
import com.poly.duantotnghiep.repositoty.ProductRepository;
import com.poly.duantotnghiep.repositoty.UserRepository;
import com.poly.duantotnghiep.service.BrandService;
import com.poly.duantotnghiep.service.ProductService;
import com.poly.duantotnghiep.utils.exception.ApplicationException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Component
public class ProductServiceImpl implements ProductService {
    @Autowired
    ProductRepository productRepository;
    @Autowired
    UserRepository userRepository;
    @Override
    public ProductResponse addProduct(BaseRequestData baseRequestData) throws ApplicationException {
        ProductResponse productResponse = new ProductResponse();
        ProductRequest productRequest = (ProductRequest) baseRequestData.getWsRequest();
        try {
            User user = userRepository.findBySessionAndToken(baseRequestData.getSessionId(), baseRequestData.getToken());
            if (ObjectUtils.isEmpty(user)) {
                throw new ApplicationException(ApplicationCode.UNAUTHORIZED);
            }
            Product product = new Product();

            BeanUtils.copyProperties(productRequest, product);
            product.setCreatedDate(LocalDateTime.now());
            product.setCreateBy(user.getUsername());

            productRepository.save(product);
            BeanUtils.copyProperties(product, productResponse);
        } catch (Exception e) {
            e.printStackTrace();
            throw  new ApplicationException(ApplicationCode.ERROR);

        }

        return productResponse;
    }


    @Override
    public ProductResponse detailProduct(BaseRequestData baseRequestData) throws ApplicationException {
        ProductResponse productResponse = new ProductResponse();
        ProductRequest productRequest = (ProductRequest) baseRequestData.getWsRequest();
        try {
            User user = userRepository.findBySessionAndToken(baseRequestData.getSessionId(), baseRequestData.getToken());
            if (ObjectUtils.isEmpty(user)) {
                throw new ApplicationException(ApplicationCode.UNAUTHORIZED);
            }
            Product product=productRepository.findById(productRequest.getProductId()).get();
            BeanUtils.copyProperties(product, productResponse);
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return productResponse;
    }


    @Override
    public ListProductResponse getListProduct(BaseRequestData baseRequestData) throws ApplicationException {
        ListProductResponse listProductResponse = new ListProductResponse();
        ProductRequest productRequest = (ProductRequest) baseRequestData.getWsRequest();
        try {
            User user = userRepository.findBySessionAndToken(baseRequestData.getSessionId(), baseRequestData.getToken());
            if (ObjectUtils.isEmpty(user)) {
                throw new ApplicationException(ApplicationCode.UNAUTHORIZED);
            }
            List<Product> productList =
                    productRepository.getListProduct(productRequest.getName(),
                            PageRequest.of(productRequest.getPage(), productRequest.getPageSize()));
            int totalItem = productRepository.countProductbyName(
                    productRequest.getName());
            int totalPage = (int) Math.ceil((double) totalItem / (double) productRequest.getPageSize());

            List<ProductResponse> productResponses = new ArrayList<>();
            for (Product product : productList) {
                ProductResponse productResponse = new ProductResponse();
                BeanUtils.copyProperties(product, productResponse);
                productResponses.add(productResponse);
            }
            listProductResponse.setTotalItem(totalItem);
            listProductResponse.setTotalPage(totalPage);
            listProductResponse.setProductList(productResponses);

        } catch (Exception e) {
            e.printStackTrace();

        }
        return listProductResponse;
    }

    @Override
    public ProductResponse updateProduct(BaseRequestData baseRequestData) throws ApplicationException {
        ProductResponse productResponse = new ProductResponse();
        ProductRequest productRequest = (ProductRequest) baseRequestData.getWsRequest();
        Product product = productRepository.findById(productRequest.getProductId()).get();

        try {
            User user = userRepository.findBySessionAndToken(baseRequestData.getSessionId(), baseRequestData.getToken());
            if (ObjectUtils.isEmpty(user)) {
                throw new ApplicationException(ApplicationCode.UNAUTHORIZED);
            }

            if (!ObjectUtils.isEmpty(product)) {
                BeanUtils.copyProperties(productRequest, product);
                product.setCreatedDate(productRequest.getCreatedDate());
                product.setModifiedDate(LocalDateTime.now());
                product.setModifiedBy(user.getModifiedBy());
                productRepository.save(product);
                BeanUtils.copyProperties(product, productResponse);
            }
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return productResponse;
}}

