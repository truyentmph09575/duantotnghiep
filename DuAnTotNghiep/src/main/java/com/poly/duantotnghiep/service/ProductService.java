package com.poly.duantotnghiep.service;

import com.poly.duantotnghiep.dto.request.BaseRequestData;

import com.poly.duantotnghiep.dto.response.product.ListProductResponse;
import com.poly.duantotnghiep.dto.response.product.ProductResponse;
import com.poly.duantotnghiep.utils.exception.ApplicationException;



public interface ProductService {
    ProductResponse addProduct(BaseRequestData baseRequestData) throws ApplicationException;
    ProductResponse detailProduct(BaseRequestData baseRequestData) throws ApplicationException;
    ListProductResponse getListProduct(BaseRequestData baseRequestData) throws ApplicationException;
    ProductResponse updateProduct(BaseRequestData baseRequestData) throws ApplicationException;

}
