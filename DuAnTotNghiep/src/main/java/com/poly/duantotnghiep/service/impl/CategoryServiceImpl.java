package com.poly.duantotnghiep.service.impl;

import com.poly.duantotnghiep.constant.ApplicationCode;
import com.poly.duantotnghiep.dto.request.BaseRequestData;
import com.poly.duantotnghiep.dto.request.brand.BrandRequest;
import com.poly.duantotnghiep.dto.request.category.CategoryRequest;
import com.poly.duantotnghiep.dto.response.brand.BrandResponse;
import com.poly.duantotnghiep.dto.response.brand.ListBrandResponse;
import com.poly.duantotnghiep.dto.response.category.CategoryResponse;
import com.poly.duantotnghiep.dto.response.category.ListCategoryResponse;
import com.poly.duantotnghiep.model.Brand;
import com.poly.duantotnghiep.model.Category;
import com.poly.duantotnghiep.model.User;
import com.poly.duantotnghiep.repositoty.BrandRepository;
import com.poly.duantotnghiep.repositoty.CategoryRepository;
import com.poly.duantotnghiep.repositoty.UserRepository;
import com.poly.duantotnghiep.service.BrandService;
import com.poly.duantotnghiep.service.CategoryService;
import com.poly.duantotnghiep.utils.exception.ApplicationException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Component
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    UserRepository userRepository;

    @Override
    public CategoryResponse addCategory(BaseRequestData baseRequestData) throws ApplicationException {
        CategoryResponse categoryResponse = new CategoryResponse();

        CategoryRequest categoryRequest = (CategoryRequest) baseRequestData.getWsRequest();
        try {
            User user = userRepository.findBySessionAndToken(baseRequestData.getSessionId(), baseRequestData.getToken());
            if (ObjectUtils.isEmpty(user)) {
                throw new ApplicationException(ApplicationCode.UNAUTHORIZED);
            }
            Category category = new Category();

            BeanUtils.copyProperties(categoryRequest, category);
            category.setCreateBy(user.getUsername());
            category.setCreatedDate(LocalDateTime.now());
            categoryRepository.save(category);
            BeanUtils.copyProperties(category, categoryResponse);
        } catch (Exception e) {
            e.printStackTrace();
            throw  new ApplicationException(ApplicationCode.ERROR);

        }

        return categoryResponse;
    }


    @Override
    public CategoryResponse detailCategory(BaseRequestData baseRequestData) throws ApplicationException {
        CategoryResponse categoryResponse = new CategoryResponse();
        CategoryRequest categoryRequest = (CategoryRequest) baseRequestData.getWsRequest();
        try {
            User user = userRepository.findBySessionAndToken(baseRequestData.getSessionId(), baseRequestData.getToken());
            if (ObjectUtils.isEmpty(user)) {
                throw new ApplicationException(ApplicationCode.UNAUTHORIZED);
            }
            Category category=categoryRepository.findById(categoryRequest.getCategoryId()).get();
            BeanUtils.copyProperties(category, categoryResponse);
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return categoryResponse;
    }


    @Override
    public ListCategoryResponse getListCategory(BaseRequestData baseRequestData) throws ApplicationException {
        ListCategoryResponse listCategoryResponse = new ListCategoryResponse();
        CategoryRequest categoryRequest = (CategoryRequest) baseRequestData.getWsRequest();
        try {
            User user = userRepository.findBySessionAndToken(baseRequestData.getSessionId(), baseRequestData.getToken());
            if (ObjectUtils.isEmpty(user)) {
                throw new ApplicationException(ApplicationCode.UNAUTHORIZED);
            }
            List<Category> categoryList =
                    categoryRepository.getListCategory(categoryRequest.getName(),
                            PageRequest.of(categoryRequest.getPage(), categoryRequest.getPageSize()));
            int totalItem = categoryRepository.countCategorybyName(
                    categoryRequest.getName());
            int totalPage = (int) Math.ceil((double) totalItem / (double) categoryRequest.getPageSize());

            List<CategoryResponse> categoryResponses = new ArrayList<>();
            for (Category category : categoryList) {
                CategoryResponse categoryResponse = new CategoryResponse();
                BeanUtils.copyProperties(category, categoryResponse);
                categoryResponses.add(categoryResponse);
            }
            listCategoryResponse.setTotalItem(totalItem);
            listCategoryResponse.setTotalPage(totalPage);
            listCategoryResponse.setCategoryList(categoryResponses);

        } catch (Exception e) {
            e.printStackTrace();

        }
        return listCategoryResponse;
    }

    @Override
    public CategoryResponse updateCategory(BaseRequestData baseRequestData) throws ApplicationException {
        CategoryResponse categoryResponse = new CategoryResponse();
        CategoryRequest categoryRequest = (CategoryRequest) baseRequestData.getWsRequest();
        Category category = categoryRepository.findById(categoryRequest.getCategoryId()).get();

        try {
            User user = userRepository.findBySessionAndToken(baseRequestData.getSessionId(), baseRequestData.getToken());
            if (ObjectUtils.isEmpty(user)) {
                throw new ApplicationException(ApplicationCode.UNAUTHORIZED);
            }
            if (!ObjectUtils.isEmpty(category)) {
                BeanUtils.copyProperties(categoryRequest, category);
                category.setCreatedDate(categoryRequest.getCreatedDate());
                category.setModifiedDate(LocalDateTime.now());
                category.setCreateBy(user.getUsername());
                category.setModifiedBy(user.getUsername());
                categoryRepository.save(category);
                BeanUtils.copyProperties(category, categoryResponse);
            }
        } catch (Exception e) {
            e.getLocalizedMessage();
        }

        return categoryResponse;
    }
}
