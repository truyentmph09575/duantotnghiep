package com.poly.duantotnghiep.utils;


import com.poly.duantotnghiep.model.City;
import com.poly.duantotnghiep.model.District;
import com.poly.duantotnghiep.model.Village;
import com.poly.duantotnghiep.repositoty.CityRepository;
import com.poly.duantotnghiep.repositoty.DistrictRepository;
import com.poly.duantotnghiep.repositoty.VillageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class Schedule {

    public static Map<String, City> cityHashMap = new HashMap<>();
    public static Map<String, District> districtHashMap = new HashMap<>();
    public static Map<String, Village> villageHashMap = new HashMap<>();



    @Autowired
    CityRepository cityRepository;
    @Autowired
    DistrictRepository districtRepository;
    @Autowired
    VillageRepository villageRepository;


    @Scheduled(fixedRate = 800000000)
    public void getListCity(){
        cityHashMap.clear();
        List<City> cityList = cityRepository.findAll();
        for (City city : cityList){
            cityHashMap.put(city.getCityId(), city);
        }
    }

    @Scheduled(fixedRate = 800000000)
    public void getListDistrist(){
        districtHashMap.clear();
        List<District> districtList = districtRepository.findAll();
        for (District district : districtList){
            districtHashMap.put(district.getDistrictId(), district);
        }
    }
    @Scheduled(fixedRate = 800000000)
    public void getListVillage(){
        villageHashMap.clear();
        List<Village> villageList = villageRepository.findAll();
        for (Village village : villageList){
            villageHashMap.put(village.getVillageId(), village);
        }
    }

}
