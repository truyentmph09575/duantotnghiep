package com.poly.duantotnghiep.controller;

import com.poly.duantotnghiep.api.baseApi.IApi;
import com.poly.duantotnghiep.dto.request.BaseRequestData;
import com.poly.duantotnghiep.dto.response.baseReponse.BaseResponseData;
import com.poly.duantotnghiep.dto.response.baseReponse.IResponseData;
import com.poly.duantotnghiep.utils.exception.ApplicationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author MaiPH
 */
public class BaseController {
    
    @Autowired
    private ApplicationContext context;

    protected BaseResponseData<IResponseData> handle(String apiName, BaseRequestData request) throws ApplicationException {
        IApi api = context.getBean(apiName, IApi.class);
        if(api == null){
        }
        return api.excute(request);
    }
}
