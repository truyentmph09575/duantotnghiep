package com.poly.duantotnghiep.controller;

import com.poly.duantotnghiep.dto.request.BaseRequestData;
import com.poly.duantotnghiep.dto.request.attribute.AttributeRequest;
import com.poly.duantotnghiep.dto.request.brand.BrandRequest;
import com.poly.duantotnghiep.dto.response.baseReponse.BaseResponseData;
import com.poly.duantotnghiep.utils.exception.ApplicationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/")
public class BrandController extends  BaseController {

    @PostMapping(value = "addBrand")
    @ResponseBody
    public ResponseEntity<?> AddBrand(@RequestBody BaseRequestData<BrandRequest> requestData) throws ApplicationException {
        BaseResponseData baseResponseData = handle("addBrand", requestData);
        return new ResponseEntity<>(baseResponseData, HttpStatus.OK);
    }


    @PostMapping(value = "updateBrand")
    @ResponseBody
    public ResponseEntity<?> UpdateBrand(@RequestBody BaseRequestData<BrandRequest> requestData) throws ApplicationException {
        BaseResponseData baseResponseData = handle("updateBrand", requestData);
        return new ResponseEntity<>(baseResponseData, HttpStatus.OK);
    }

    @PostMapping(value = "detailBrand")
    @ResponseBody
    public ResponseEntity<?> DetailBrand(@RequestBody BaseRequestData<BrandRequest> requestData) throws ApplicationException {
        BaseResponseData baseResponseData = handle("detailBrand", requestData);
        return new ResponseEntity<>(baseResponseData, HttpStatus.OK);
    }

    @PostMapping(value = "getListBrand")
    @ResponseBody
    public ResponseEntity<?> GetListBrand(@RequestBody BaseRequestData<BrandRequest> requestData) throws ApplicationException {
        BaseResponseData baseResponseData = handle("getListBrand", requestData);
        return new ResponseEntity<>(baseResponseData, HttpStatus.OK);
    }
}