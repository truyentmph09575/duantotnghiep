package com.poly.duantotnghiep.controller;


import com.poly.duantotnghiep.dto.request.BaseRequestData;
import com.poly.duantotnghiep.dto.request.cart.CartRequest;
import com.poly.duantotnghiep.dto.response.baseReponse.BaseResponseData;
import com.poly.duantotnghiep.repositoty.UserRepository;
import com.poly.duantotnghiep.utils.exception.ApplicationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/")
public class CartController extends BaseController {
    @Autowired
    UserRepository userRepository;
    @PostMapping("addCart")
    @ResponseBody
    public ResponseEntity<?> addCart(@RequestBody BaseRequestData<CartRequest> requestData) throws ApplicationException {
        BaseResponseData baseResponseData = new BaseResponseData();
        baseResponseData = handle("addCart", requestData);
        return new ResponseEntity<>(baseResponseData, HttpStatus.OK);
    }

    @PostMapping("getListCart")
    @ResponseBody
    public ResponseEntity<?> getListCart(@RequestBody BaseRequestData<CartRequest> requestData) throws ApplicationException{
        BaseResponseData baseResponseData= new BaseResponseData();
        baseResponseData = handle("getListCart", requestData);
        return  new ResponseEntity<>(baseResponseData, HttpStatus.OK);
    }

    @PostMapping("deleteCart")
    @ResponseBody
    public ResponseEntity<?> deleteCart(@RequestBody BaseRequestData<CartRequest> requestData) throws ApplicationException{
        BaseResponseData baseResponseData= new BaseResponseData();

        baseResponseData = handle("deleteCart", requestData);
        return  new ResponseEntity<>(baseResponseData, HttpStatus.OK);
    }
}
