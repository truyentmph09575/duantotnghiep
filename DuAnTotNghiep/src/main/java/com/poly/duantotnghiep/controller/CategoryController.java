package com.poly.duantotnghiep.controller;

import com.poly.duantotnghiep.dto.request.BaseRequestData;
import com.poly.duantotnghiep.dto.request.category.CategoryRequest;
import com.poly.duantotnghiep.dto.response.baseReponse.BaseResponseData;
import com.poly.duantotnghiep.utils.exception.ApplicationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/")
public class CategoryController extends  BaseController {

    @PostMapping(value = "addCategory")
    @ResponseBody
    public ResponseEntity<?> AddCategory(@RequestBody BaseRequestData<CategoryRequest> requestData) throws ApplicationException {
        BaseResponseData baseResponseData = handle("addCategory", requestData);
        return new ResponseEntity<>(baseResponseData, HttpStatus.OK);
    }


    @PostMapping(value = "updateCategory")
    @ResponseBody
    public ResponseEntity<?> UpdateCategory(@RequestBody BaseRequestData<CategoryRequest> requestData) throws ApplicationException {
        BaseResponseData baseResponseData = handle("updateCategory", requestData);
        return new ResponseEntity<>(baseResponseData, HttpStatus.OK);
    }

    @PostMapping(value = "detailCategory")
    @ResponseBody
    public ResponseEntity<?> DetailCategory(@RequestBody BaseRequestData<CategoryRequest> requestData) throws ApplicationException {
        BaseResponseData baseResponseData = handle("detailCategory", requestData);
        return new ResponseEntity<>(baseResponseData, HttpStatus.OK);
    }

    @PostMapping(value = "getListCategory")
    @ResponseBody
    public ResponseEntity<?> GetListCategory(@RequestBody BaseRequestData<CategoryRequest> requestData) throws ApplicationException {
        BaseResponseData baseResponseData = handle("getListCategory", requestData);
        return new ResponseEntity<>(baseResponseData, HttpStatus.OK);
    }
}
