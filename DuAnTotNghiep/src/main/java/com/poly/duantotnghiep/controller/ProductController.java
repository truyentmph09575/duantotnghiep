package com.poly.duantotnghiep.controller;

import com.poly.duantotnghiep.dto.request.BaseRequestData;
import com.poly.duantotnghiep.dto.request.brand.BrandRequest;
import com.poly.duantotnghiep.dto.request.product.ProductRequest;
import com.poly.duantotnghiep.dto.response.baseReponse.BaseResponseData;
import com.poly.duantotnghiep.utils.exception.ApplicationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/")
public class ProductController extends  BaseController {

    @PostMapping(value = "addProduct")
    @ResponseBody
    public ResponseEntity<?> AddProduct(@RequestBody BaseRequestData<ProductRequest> requestData) throws ApplicationException {
        BaseResponseData baseResponseData = handle("addProduct", requestData);
        return new ResponseEntity<>(baseResponseData, HttpStatus.OK);
    }


    @PostMapping(value = "updateProduct")
    @ResponseBody
    public ResponseEntity<?> UpdateProduct(@RequestBody BaseRequestData<ProductRequest> requestData) throws ApplicationException {
        BaseResponseData baseResponseData = handle("updateProduct", requestData);
        return new ResponseEntity<>(baseResponseData, HttpStatus.OK);
    }

    @PostMapping(value = "detailProduct")
    @ResponseBody
    public ResponseEntity<?> DetailProduct(@RequestBody BaseRequestData<ProductRequest> requestData) throws ApplicationException {
        BaseResponseData baseResponseData = handle("detailProduct", requestData);
        return new ResponseEntity<>(baseResponseData, HttpStatus.OK);
    }

    @PostMapping(value = "getListProduct")
    @ResponseBody
    public ResponseEntity<?> GetListProduct(@RequestBody BaseRequestData<ProductRequest> requestData) throws ApplicationException {
        BaseResponseData baseResponseData = handle("getListProduct", requestData);
        return new ResponseEntity<>(baseResponseData, HttpStatus.OK);
    }
}
