package com.poly.duantotnghiep.controller;

import com.poly.duantotnghiep.dto.request.BaseRequestData;
import com.poly.duantotnghiep.dto.request.user.UserRequest;
import com.poly.duantotnghiep.dto.response.baseReponse.BaseResponseData;
import com.poly.duantotnghiep.utils.exception.ApplicationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController

@RequestMapping("/api/")
public class UserController extends BaseController{
    @PostMapping(value = "addUser")
    @ResponseBody
    public ResponseEntity<?> addUser(@RequestBody BaseRequestData<UserRequest> request) throws ApplicationException {
        BaseResponseData response = handle("addUser", request);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @PostMapping(value = "loginCMS")
    @ResponseBody
    public ResponseEntity<?> loginCMS(@RequestBody BaseRequestData<UserRequest> request) throws ApplicationException {
        BaseResponseData response = handle("loginCMS", request);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @PostMapping(value = "updateUser")
    @ResponseBody
    public ResponseEntity<?> updateUser(@RequestBody BaseRequestData<UserRequest> request) throws ApplicationException {
        BaseResponseData response = handle("updateUser", request);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @PostMapping(value = "deleteUser")
    @ResponseBody
    public ResponseEntity<?> deleteUser(@RequestBody BaseRequestData<UserRequest> request) throws ApplicationException {
        BaseResponseData response = handle("deleteUser", request);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @PostMapping(value = "detailUser")
    @ResponseBody
    public ResponseEntity<?> detailUser(@RequestBody BaseRequestData<UserRequest> request) throws ApplicationException {
        BaseResponseData response = handle("detailUser", request);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @PostMapping(value = "getListUser")
    @ResponseBody
    public ResponseEntity<?> getListUser(@RequestBody BaseRequestData<UserRequest> request) throws ApplicationException {
        BaseResponseData response = handle("getListUser", request);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
