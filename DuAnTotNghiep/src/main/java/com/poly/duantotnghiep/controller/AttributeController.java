package com.poly.duantotnghiep.controller;

import com.poly.duantotnghiep.dto.request.BaseRequestData;

import com.poly.duantotnghiep.dto.request.attribute.AttributeRequest;
import com.poly.duantotnghiep.dto.response.baseReponse.BaseResponseData;
import com.poly.duantotnghiep.utils.exception.ApplicationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/")
public class AttributeController extends  BaseController {

    @PostMapping(value = "addAttribute")
    @ResponseBody
    public ResponseEntity<?> AddAttribute(@RequestBody BaseRequestData<AttributeRequest> requestData) throws ApplicationException {
        BaseResponseData baseResponseData = handle("addAttribute", requestData);
        return new ResponseEntity<>(baseResponseData, HttpStatus.OK);
    }


    @PostMapping(value = "updateAttribute")
    @ResponseBody
    public ResponseEntity<?> UpdateAttribute(@RequestBody BaseRequestData<AttributeRequest> requestData) throws ApplicationException {
        BaseResponseData baseResponseData = handle("updateAttribute", requestData);
        return new ResponseEntity<>(baseResponseData, HttpStatus.OK);
    }

    @PostMapping(value = "detailAttribute")
    @ResponseBody
    public ResponseEntity<?> DetailAttribute(@RequestBody BaseRequestData<AttributeRequest> requestData) throws ApplicationException {
        BaseResponseData baseResponseData = handle("detailAttribute", requestData);
        return new ResponseEntity<>(baseResponseData, HttpStatus.OK);
    }

    @PostMapping(value = "getListAttribute")
    @ResponseBody
    public ResponseEntity<?> GetListAttribute(@RequestBody BaseRequestData<AttributeRequest> requestData) throws ApplicationException {
        BaseResponseData baseResponseData = handle("getListAttribute", requestData);
        return new ResponseEntity<>(baseResponseData, HttpStatus.OK);
    }
}

