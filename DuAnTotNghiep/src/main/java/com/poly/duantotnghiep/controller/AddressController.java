package com.poly.duantotnghiep.controller;

import com.poly.duantotnghiep.dto.request.BaseRequestData;
import com.poly.duantotnghiep.dto.request.address.CityRequest;
import com.poly.duantotnghiep.dto.request.address.DistrictRequest;
import com.poly.duantotnghiep.dto.request.address.VillageRequest;
import com.poly.duantotnghiep.dto.response.baseReponse.BaseResponseData;
import com.poly.duantotnghiep.utils.exception.ApplicationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/")
public class AddressController extends  BaseController {

	@PostMapping(value = "listCity")
	@ResponseBody
	public ResponseEntity<?> ListCity(@RequestBody BaseRequestData<CityRequest> requestData) throws ApplicationException {
		BaseResponseData baseResponseData = handle("listCity", requestData);
		return new ResponseEntity<>(baseResponseData, HttpStatus.OK);
	}

	@PostMapping(value = "listDistrict")
	@ResponseBody
	public ResponseEntity<?> ListDistrict(@RequestBody BaseRequestData<DistrictRequest> requestData) throws ApplicationException {
		BaseResponseData baseResponseData = handle("listDistrict", requestData);
		return new ResponseEntity<>(baseResponseData, HttpStatus.OK);
	}

	@PostMapping(value = "listVillage")
	@ResponseBody
	public ResponseEntity<?> ListVillage(@RequestBody BaseRequestData<VillageRequest> requestData) throws ApplicationException {
		BaseResponseData baseResponseData = handle("listVillage", requestData);
		return new ResponseEntity<>(baseResponseData, HttpStatus.OK);
	}

	@PostMapping(value = "detailCity")
	@ResponseBody
	public ResponseEntity<?> City(@RequestBody BaseRequestData<CityRequest> requestData) throws ApplicationException {
		BaseResponseData baseResponseData = handle("detailCity", requestData);
		return new ResponseEntity<>(baseResponseData, HttpStatus.OK);
	}

	@PostMapping(value = "detailDistrict")
	@ResponseBody
	public ResponseEntity<?> District(@RequestBody BaseRequestData<DistrictRequest> requestData) throws ApplicationException {
		BaseResponseData baseResponseData = handle("detailCity", requestData);
		return new ResponseEntity<>(baseResponseData, HttpStatus.OK);
	}

	@PostMapping(value = "detailVillage")
	@ResponseBody
	public ResponseEntity<?> Village(@RequestBody BaseRequestData<VillageRequest> requestData) throws ApplicationException {
		BaseResponseData baseResponseData = handle("detailCity", requestData);
		return new ResponseEntity<>(baseResponseData, HttpStatus.OK);
	}

}
