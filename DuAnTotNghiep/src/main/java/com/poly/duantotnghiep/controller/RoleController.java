package com.poly.duantotnghiep.controller;

import com.poly.duantotnghiep.dto.request.BaseRequestData;
import com.poly.duantotnghiep.dto.request.role.RoleRequest;
import com.poly.duantotnghiep.dto.response.baseReponse.BaseResponseData;
import com.poly.duantotnghiep.utils.exception.ApplicationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController

@RequestMapping("/api/")
public class RoleController extends BaseController{
    @PostMapping(value = "addRole")
    @ResponseBody
    public ResponseEntity<?> addRole(@RequestBody BaseRequestData<RoleRequest> request) throws ApplicationException {
        BaseResponseData response = handle("addRole", request);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
