package com.poly.duantotnghiep.dto.response.role;

import com.poly.duantotnghiep.dto.response.baseReponse.IResponseData;
import com.poly.duantotnghiep.model.Role;
import com.poly.duantotnghiep.model.User;
import lombok.Data;

import java.util.List;
@Data
public class ListRoleRepone implements IResponseData {
    List<Role> userListRole;
    private int totalItem;
    private int totalPage;
}
