package com.poly.duantotnghiep.dto.request.product;


import com.poly.duantotnghiep.dto.request.IRequestData;
import com.poly.duantotnghiep.model.Attribute;
import com.poly.duantotnghiep.model.Product;
import lombok.Data;

@Data
public class ProductRequest  extends Product implements IRequestData {
  private   int page;
  private   int pageSize;
  private   String textSearch;
    @Override
    public boolean isValid() {
        return false;
    }
}
