package com.poly.duantotnghiep.dto.response.product;

import com.poly.duantotnghiep.dto.response.baseReponse.IResponseData;
import lombok.Data;

import java.util.List;
@Data
public class ListProductResponse implements IResponseData {
    int totalItem;
    int totalPage;

    List<ProductResponse> productList;
}

