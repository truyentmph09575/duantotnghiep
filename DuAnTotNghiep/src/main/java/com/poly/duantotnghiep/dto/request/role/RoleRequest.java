package com.poly.duantotnghiep.dto.request.role;

import com.poly.duantotnghiep.dto.request.IRequestData;
import com.poly.duantotnghiep.model.Role;
import lombok.Data;

@Data
public class RoleRequest extends Role implements IRequestData {

    @Override
    public boolean isValid() {
        return false;
    }
}
