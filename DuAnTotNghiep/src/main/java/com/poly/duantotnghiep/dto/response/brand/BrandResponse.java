package com.poly.duantotnghiep.dto.response.brand;

import com.poly.duantotnghiep.dto.response.baseReponse.IResponseData;

import com.poly.duantotnghiep.model.Brand;
import lombok.Data;

@Data
public class BrandResponse extends Brand implements IResponseData {
}
