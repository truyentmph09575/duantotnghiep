package com.poly.duantotnghiep.dto.response.address;

import com.poly.duantotnghiep.dto.response.baseReponse.IResponseData;
import lombok.Data;

import java.util.List;

@Data
public class ListCityResponse implements IResponseData {
	List<CityResponse> cityResponseList;
}
