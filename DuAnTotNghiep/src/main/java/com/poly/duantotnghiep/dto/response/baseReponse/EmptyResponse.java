package com.poly.duantotnghiep.dto.response.baseReponse;

import lombok.Data;

/**
 * EmptyResponse
 *
 * @author Chidq
 */
@Data
public class EmptyResponse implements IResponseData {
    int message;
}
