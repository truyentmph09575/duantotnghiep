package com.poly.duantotnghiep.dto.request.address;


import com.poly.duantotnghiep.dto.request.IRequestData;
import com.poly.duantotnghiep.model.Village;
import lombok.Data;

@Data
public class VillageRequest extends Village implements IRequestData {

	@Override
	public boolean isValid() {
		return false;
	}
}
