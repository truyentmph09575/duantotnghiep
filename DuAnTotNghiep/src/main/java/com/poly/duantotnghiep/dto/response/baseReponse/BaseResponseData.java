package com.poly.duantotnghiep.dto.response.baseReponse;

import lombok.Data;

/**
 * BaseResponseData
 *
 * @author Chidq
 */
@Data
public class BaseResponseData<T extends IResponseData> {
    int errorCode;
    String message;
    T wsResponse;


}
