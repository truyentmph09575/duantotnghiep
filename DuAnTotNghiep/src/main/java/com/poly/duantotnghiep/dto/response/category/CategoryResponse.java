package com.poly.duantotnghiep.dto.response.category;

import com.poly.duantotnghiep.dto.response.baseReponse.IResponseData;
import com.poly.duantotnghiep.model.Brand;
import com.poly.duantotnghiep.model.Category;
import lombok.Data;

@Data
public class CategoryResponse extends Category implements IResponseData {
}
