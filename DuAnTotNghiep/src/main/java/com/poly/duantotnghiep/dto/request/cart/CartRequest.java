package com.poly.duantotnghiep.dto.request.cart;


import com.poly.duantotnghiep.dto.request.IRequestData;
import com.poly.duantotnghiep.model.Cart;
import lombok.Data;

@Data
public class CartRequest extends Cart implements IRequestData {
    int page;
    int pageSize;
    @Override
    public boolean isValid() {
        return false;
    }
}
