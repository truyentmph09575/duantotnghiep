package com.poly.duantotnghiep.dto.response.cart;


import com.poly.duantotnghiep.dto.response.baseReponse.IResponseData;
import lombok.Data;

import java.util.List;

@Data
public class ListCartRespone implements IResponseData {
    int totalPage;
    int totalItem;
    double totalPrice;
    List<CartRespone> cartResponeList;
}
