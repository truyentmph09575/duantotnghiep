package com.poly.duantotnghiep.dto.response.user;

import com.poly.duantotnghiep.dto.response.baseReponse.IResponseData;
import com.poly.duantotnghiep.model.User;
import lombok.Data;

import java.util.List;
@Data
public class ListUserRepone implements IResponseData {
    List<UserReponse> userListResponse;
    private int totalItem;
    private int totalPage;
}
