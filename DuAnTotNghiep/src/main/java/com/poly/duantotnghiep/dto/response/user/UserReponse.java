package com.poly.duantotnghiep.dto.response.user;

import com.poly.duantotnghiep.dto.response.baseReponse.IResponseData;
import com.poly.duantotnghiep.model.User;
import lombok.Data;

@Data
public class UserReponse  extends User implements IResponseData {
    private String nameRole;
}
