package com.poly.duantotnghiep.dto.request.address;

import com.poly.duantotnghiep.dto.request.IRequestData;
import com.poly.duantotnghiep.model.City;
import lombok.Data;

@Data
public class CityRequest extends City implements IRequestData {

	@Override
	public boolean isValid() {
		return false;
	}
}
