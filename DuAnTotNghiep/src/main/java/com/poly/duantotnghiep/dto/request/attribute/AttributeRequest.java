package com.poly.duantotnghiep.dto.request.attribute;

import com.poly.duantotnghiep.dto.request.IRequestData;
import com.poly.duantotnghiep.model.Attribute;
import lombok.Data;

@Data
public class AttributeRequest  extends Attribute implements IRequestData {
   private int page;
   private int pageSize;
   private String textSearch;
    @Override
    public boolean isValid() {
        return false;
    }
}
