package com.poly.duantotnghiep.dto.request.address;


import com.poly.duantotnghiep.dto.request.IRequestData;
import com.poly.duantotnghiep.model.District;
import lombok.Data;

@Data
public class DistrictRequest extends District implements IRequestData {

	@Override
	public boolean isValid() {
		return false;
	}
}
