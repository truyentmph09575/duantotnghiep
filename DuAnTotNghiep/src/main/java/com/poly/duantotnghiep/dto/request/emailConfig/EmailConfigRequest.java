package com.poly.duantotnghiep.dto.request.emailConfig;


import com.poly.duantotnghiep.dto.request.IRequestData;
import com.poly.duantotnghiep.model.EmailConfig;
import lombok.Data;

@Data
public class EmailConfigRequest extends EmailConfig implements IRequestData {
    @Override
    public boolean isValid() {
        return false;
    }
}
