package com.poly.duantotnghiep.dto.response.brand;

import com.poly.duantotnghiep.dto.response.baseReponse.IResponseData;
import com.poly.duantotnghiep.model.Attribute;
import com.poly.duantotnghiep.model.Brand;
import lombok.Data;

import java.util.List;

@Data
public class ListBrandResponse implements IResponseData {
    int totalItem;
    int totalPage;

    List<BrandResponse> brandList;
}
