package com.poly.duantotnghiep.dto.request.brand;

import com.poly.duantotnghiep.dto.request.IRequestData;

import com.poly.duantotnghiep.model.Brand;
import lombok.Data;

@Data
public class BrandRequest  extends Brand implements IRequestData {
   private int page;
   private int pageSize;
   private String textSearch;
    @Override
    public boolean isValid() {
        return false;
    }
}
