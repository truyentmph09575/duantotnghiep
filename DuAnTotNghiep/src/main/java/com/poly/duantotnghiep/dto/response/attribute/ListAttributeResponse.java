package com.poly.duantotnghiep.dto.response.attribute;

import com.poly.duantotnghiep.dto.response.baseReponse.IResponseData;

import com.poly.duantotnghiep.model.Attribute;
import lombok.Data;

import java.util.List;


@Data
public class ListAttributeResponse implements IResponseData {
    int totalItem;
    int totalPage;
    List<AttributeResponse> attributeList;
}
