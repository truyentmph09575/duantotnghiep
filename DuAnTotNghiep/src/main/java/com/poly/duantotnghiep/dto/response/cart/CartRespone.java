package com.poly.duantotnghiep.dto.response.cart;

import com.poly.duantotnghiep.dto.response.baseReponse.IResponseData;
import com.poly.duantotnghiep.model.Cart;
import lombok.Data;

@Data
public class CartRespone extends Cart implements IResponseData {
}
