package com.poly.duantotnghiep.dto.response.category;

import com.poly.duantotnghiep.dto.response.baseReponse.IResponseData;
import com.poly.duantotnghiep.dto.response.brand.BrandResponse;
import lombok.Data;

import java.util.List;

@Data
public class ListCategoryResponse implements IResponseData {
    int totalItem;
    int totalPage;

    List<CategoryResponse> categoryList;
}
