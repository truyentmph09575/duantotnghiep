package com.poly.duantotnghiep.dto.request.user;

import com.poly.duantotnghiep.dto.request.IRequestData;
import com.poly.duantotnghiep.model.User;
import lombok.Data;

@Data
public class UserRequest extends User implements IRequestData {
    private int pageSize;
    private int page;
    private String newPass;
    @Override
    public boolean isValid() {
        return false;
    }
}
