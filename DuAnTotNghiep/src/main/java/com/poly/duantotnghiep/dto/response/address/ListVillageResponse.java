package com.poly.duantotnghiep.dto.response.address;


import com.poly.duantotnghiep.dto.response.baseReponse.IResponseData;
import lombok.Data;

import java.util.List;

@Data
public class ListVillageResponse implements IResponseData {
	List<VillageResponse> villageResponseList;
}
