package com.poly.duantotnghiep.dto.response.attribute;

import com.poly.duantotnghiep.dto.response.baseReponse.IResponseData;
import com.poly.duantotnghiep.model.Attribute;
import lombok.Data;

@Data
public class AttributeResponse extends Attribute implements IResponseData {
}
