package com.poly.duantotnghiep.dto.response.role;

import com.poly.duantotnghiep.dto.response.baseReponse.IResponseData;
import com.poly.duantotnghiep.model.Role;
import lombok.Data;

@Data
public class RoleRepone extends Role implements IResponseData {
}
