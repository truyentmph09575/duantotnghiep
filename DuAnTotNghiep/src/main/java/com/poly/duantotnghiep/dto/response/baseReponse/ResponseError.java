package com.poly.duantotnghiep.dto.response.baseReponse;

import lombok.Data;

/**
 *
 * @author Chidq
 */
@Data
public class ResponseError {
    
    private int code;
    private String message;
}
