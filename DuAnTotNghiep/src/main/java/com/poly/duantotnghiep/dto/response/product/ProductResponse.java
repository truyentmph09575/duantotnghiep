package com.poly.duantotnghiep.dto.response.product;

import com.poly.duantotnghiep.dto.response.baseReponse.IResponseData;
import com.poly.duantotnghiep.model.Brand;
import com.poly.duantotnghiep.model.Product;
import lombok.Data;

@Data
public class ProductResponse extends Product implements IResponseData {
}
