package com.poly.duantotnghiep.dto.request.category;

import com.poly.duantotnghiep.dto.request.IRequestData;
import com.poly.duantotnghiep.model.Brand;
import com.poly.duantotnghiep.model.Category;
import lombok.Data;

@Data
    public class CategoryRequest  extends Category implements IRequestData {
   private int page;
   private int pageSize;
   private String textSearch;
    @Override
    public boolean isValid() {
        return false;
    }
}