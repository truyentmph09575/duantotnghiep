package com.poly.duantotnghiep.dto.request;

/**
 *
 * @author Chidq
 */
public interface IRequestData {
    boolean isValid();
}
